
## Identifying users

Until now, many Atlassian Connect APIs used personal data such as *usernames* and *email address* to identify users. These APIs are being changed and only Atlassian Account IDs will be used to identify users. As a result, apps need to use Atlassian account IDs for identifying users, in place of current identifiers. Any data stores used by the app also needs to be updated to use Atlassian Account IDs for identifying users instead of other personal data such as usernames and/or email addresses.


## Opting in to the new behavior

For the duration of the deprecation period, some APIs are configured to accept both "legacy" (i.e. using personal data) and "new" behavior. This is to allow vendors the opportunity to incrementally move away from relying on personal data, rather than forcing all apps to switch to new behavior at a specific date. This also allows app vendors to test whether their apps work with the new APIs.

To enable app to communicate to connect their interest in the new APIs Atlassian Connect has introduced new [API migration Opt-in mechanism](../connect-api-migration/). If an app does not explicitly opt into the GDPR API migration, then it will be subject to the default product behavior. This means that our APIs will continue operating as is (potentially sending and receiving personal data), until the end of the deprecation period *29 Mar 2019* after which personal data will no long be sent or accepted. Note that if an app will not be updated to handle data format free of personal data - this app will stop working. That is why an action must be taken by app vendor to update existing Connect apps even if a particular app does not use or require personal data.


## Personal Data Visibility Semantics

In order to provide users with greater control over their own  personal data (PD) and to simplify the processing of identifying where user PD is stored/transfer and how it is  deleted if/when a user wishes to be forgotten, Atlassian products (specifically Jira, Confluence and Bitbucket) are removing user profile data from product databases. This change affects the way user's PD is accessed. Users are given direct control over visibility of their personal data and can chose to restrict all access to it. That includes Connect Apps. By default all apps (no matter the authorization model or account type), apps just get access to PD that has been set by uses as *public*.
Apps that use [user impersonation](../oauth-2-jwt-bearer-token-authorization-grant-type/) are also subject to this changes.

If an app requires access to user's non-public PD then users can do a 3LO flow in which they *explicitly* consent access to their own private PD. See [OAuth 2.0 authorization code grants (3LO) for apps](../oauth-2-authorization-code-grants-3lo-for-apps/) to learn more about how it works.

It is important to mention that if app only requires access to the currently logged in user on the front-end then it might be sufficient to use [AP.request()](../jsapi/request/). Requestor is always the logged in user and cannot be changed by an app.


# Migrating existing app

All Connect Apps regardless of their use of personal data must be updated to support new APIs and data formats. Old APIs will not be accessible after the end of migration period.
Main areas affected by changes are *inbound authorization (app → product)* , *outbound authentication (product → app)*, *iframes*, *App lifecycle*, *Webhooks*, *REST API* and *Javascript API*.

### For inbound authorization

OAuth 2 JWT user impersonation sub claim is currently user key. That is why [OAuth 2.0 JWT Bearer token authorization grant type](../oauth-2-jwt-bearer-token-authorization-grant-type/) has been updated to support an additional method for identifying users in the {{sub}} claim of the JWT assertion created by the app. In addition to accepting the deprecated user key (with the URN namespace {{urn:atlassian:connect:userkey}}), accept an account ID.
In order to use OAuth 2.0 JWT Bearer token authorization grant type Apps need to change `sub` claim to be account ID.

### For outbound authorization
User context [claim](../understanding-jwt/#a-name-token-structure-claims-a-claims) of JWT currently contains user key, user name and user display name. During the deprecation period we are adding account ID here. After the deprecation period the `claim` will be removed altogether, since the sub claim will contain the account ID.

### IFrames
Standard iframe context parameters: `user_id`, `user_key`, `loc`, `tz` will be removed.
Apps can currently define their URLs with context parameters `profileUser.name` and `profileUser.key`. A new context parameter `profileUser.accountId` will be introduced and old parameters will be deprecated.

### Remote Addon Themes
The context parameters `username` and `userkey` will be removed. Instead the app should use ```Atlassian Account Id``` from the JWT token.

### Blueprints
Confluence sends some information related to the blueprint in the body of the request during the creation process. This information will not contain `userKey` and `userLocale` anymore. Instead the app should use ```Atlassian Account Id``` from the JWT token.


### Macros
In order to help apps with migration to GDPR compliance, a new parameter ```Atlassian Account Id``` (if available) will be added to the set of ```username``` typed parameters. That should help app vendors to
start using accountId and move away from usernames.

* When your app opted into GDPR migration during deprecation period, the value of each ```username``` parameter will be replaced by accountId. This behaviour will be enforced after the end of deprecation period.
* For apps that have not yet opted-into GDPR migration during the deprecation period, a temporary companion ```accountId``` is added. It can be referred to by adding a suffix ```.accountId``` to the original parameter name. For example: if your original url request parameter looks like ```user={user}```, an extra ```user.accountId={user.accountId}``` is available for you app.

### App lifecycle / Webhooks
The installed and uninstalled [lifecycle callbacks](../app-descriptor/#lifecycle) accidentally include the query parameter `user_key`. This field will be removed. Apps using JWT authentication should instead use the user context from the JWT token.

### REST API
Currently the response of the [addons REST resource](/platform/marketplace/license-api-for-cloud-apps/) contains an array of host contacts, including the *email* and *name* of each contact. The *email* and *name* of each contact will be replaced by account ID.

### Javascript API
`AP.user.getUser()` currently returns the logged in user's ID, key and full name. This method will be deprecated and replaced by `AP.user.getCurrentUser()` which will simply return the *Atlassian Account ID*.


To help with apps migration we have already updated [Atlassian connect frameworks](../frameworks-and-tools/)

## Migrating Atlassian Connect Express Apps

If you are using [Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/) for your app you will need to update to the version `3.2.0` or above.
New version already includes all code changes to handle Atlassian Account Id in place of user id. Note that for existing installations a data migrations (described below) is also required.

## Migrating SpringBoot Apps

If you are using [Atlassian SpringBoot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/src) for your app you will need to update to the version `1.5.0` or above.
New version already includes all code changes to handle Atlassian Account Id in place of user id. Note that for existing installations a data migrations (described below) is also required.

## Data migration

In order to migrate existing data, apps can use the REST resource [/rest/api/2/user/](../rest/v2/#api-api-2-user-get) on the Atlassian application instance (Jira, Confluence, or Bitbucket) to retrieve user details, and update data in their store with the Atlassian account ID of each user. For example, here is a high level migration guide:

* Alter the data store to add an additional field or column for the Atlassian account ID. These IDs are between 1-128 characters long, and contain alphanumeric characters as well as "-" and ":" characters.
* Create a migration code that loops over all records in the data store that:
  * Identifies a user record by username or email address
  * For each record, makes a request to [/rest/api/2/user/](../rest/v2/#api-api-2-user-get) with the relevant data
  * Stores the Atlassian account ID returned in the response into the data store record
* Alter the data store to remove the username or email address field/column

## API Changes

See [APIs affected by GDPR API Migration](../api-changes-for-user-privacy-announcement-connect/) for the detailed list of APIs changes.
