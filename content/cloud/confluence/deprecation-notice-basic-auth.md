---
title: "Deprecation notice - Basic authentication with passwords"
platform: cloud
product: confcloud
category: devguide
subcategory: updates
date: "2018-07-10"
---

# Deprecation notice - Basic authentication with passwords

<div class="aui-message note">
    <div class="icon"></div>
    <p>
        <strong>The six month deprecation period for these changes began on July 10, 2018.</strong>
    </p>
    </br>
    <p>We plan to remove support for these authentication methods by January 10, 2019. Follow the CONFCLOUD tickets linked below to stay up to date.
    </p>
    <br/>
</div>

Atlassian has introduced support for [API tokens] for all Atlassian Cloud sites as a replacement 
for [basic authentication] requests that previously used a password or primary credential for an Atlassian 
account.

Basic authentication with passwords is now deprecated and will be removed early in 2019 per [Atlassian REST API policy].

## What is changing?

If you currently rely on a script, integration, or application that makes requests to Confluence Cloud with basic authentication, you should update it to use basic authentication with an API token, OAuth, or Atlassian Connect as soon as 
possible.

[Learn more about creating API tokens].

## Why is Atlassian making this change?

Using primary account credentials like passwords outside of Atlassian's secure login screens creates security risks for our 
customers. API tokens allow users to generate unique one-off tokens for each app or integration. If needed, users can easily revoke API tokens.

Furthermore, as customers adopt features like two-factor authentication and SAML, passwords may not work in all cases. Atlassian designed API tokens as an alternative for Atlassian accounts in organizations that use these features.

## Which APIs and methods will be restricted?

For the following APIs and pages, all requests using basic authentication with a non-API token credential will return 401 (Unauthorized) after the deprecation period:

* Confluence Cloud public REST API
* All Confluence Cloud web pages

### What should I watch to receive further updates?

Please watch [CONFCLOUD-61680] to be notified of changes to basic authentication.

[API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
[basic authentication]: /cloud/confluence/basic-auth-for-rest-apis/
[Atlassian REST API policy]: /platform/marketplace/atlassian-rest-api-policy/
[Learn more about creating API tokens]: https://confluence.atlassian.com/cloud/api-tokens-938839638.html
[CONFCLOUD-61680]: https://jira.atlassian.com/browse/CONFCLOUD-61680