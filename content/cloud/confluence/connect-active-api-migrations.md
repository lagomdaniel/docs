---
title: "Connect API migration"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
date: "2019-02-25"
aliases:
- /cloud/confluence/connect-active-api-migrations.html
- /cloud/confluence/connect-active-api-migrations.md
---
{{< include path="docs/content/cloud/connect/reference/active-api-migrations.snippet.md">}}
