---
title: "Atlassian Connect App migration guide to improve user privacy"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: guides
date: "2018-09-01"
aliases:
- /cloud/confluence/connect-app-migration-guide.html
- /cloud/confluence/connect-app-migration-guide.md
---
{{< include path="docs/content/cloud/connect/guides/connect-app-gdpr-migrations-header.snippet.md">}}

{{% note %}}
### Some changes are still in development

We always strive to give our vendors as much time as possible to plan and do the required work. This is why we have published this migration guide before all changes have been release into production.
See [APIs affected by GDPR API Migration](../api-changes-for-user-privacy-announcement-connect/) for the detailed list and implementation status of APIs changes.
*It is advised not enable the Connect GDPR API migration for existing Marketplace-listed production versions. We will continue to add API changes under the API migration opt-in, so apps may break. Check this page to find out when it is safe to enable the API migration for your customers.*

Need some help? Head for the developer community for [Confluence Cloud](https://community.developer.atlassian.com/t/confluence-cloud-rest-api-privacy-updates-and-migration-guide/24167/2).
{{% /note %}}

{{< include path="docs/content/cloud/confluence/snippets/confluence-connect-app-gdpr-migrations.snippet.md">}}
