---
title: "Security overview"
platform: cloud
product: confcloud
category: devguide
subcategory: security
date: "2018-07-10"
---
# Security overview

Implementing security is an essential part of integrating with Confluence Cloud. It lets Atlassian applications protect customer data from unauthorized access and from malicious or accidental changes. It also allows administrators to install apps with confidence, letting users enjoy the benefits of apps in a secure manner.

There are two parts to securing your Confluence app or integration: authentication and authorization. Authentication tells Confluence Cloud the identity of your app, authorization determines what actions it can take within Confluence.

## Authentication

Authentication is the process of identifying your app or integration to the Atlassian application and is the basis for all other security. Confluence Cloud offers several authentication patterns, depending on whether you are building an Atlassian Connect app or calling the Confluence REST APIs directly.

### Authentication for apps

Atlassian Connect uses [JWT](https://jwt.io/) (JSON Web Tokens) to authenticate apps. This is built into the supported [Atlassian Connect libraries](/cloud/jira/platform/frameworks-and-tools/).

When an app is installed, a security context is exchanged with the application. This context is used to create and validate JWT tokens, embedded in API calls. The use of JWT tokens guarantees that:

* Confluence Cloud can verify it is talking to the app, and vice versa (authenticity).
* None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method, were altered in transit (integrity).

To learn more, read our page on [authentication for apps](/cloud/confluence/authentication-for-apps).

### Authentication for REST API requests

Confluence can use one of the following four methods to authenticate clients directly: 

- **Basic authentication:** Basic authentication uses a predefined set of user credentials to authenticate the client. We recommend that you don't use basic authentication, except for tools like personal scripts or bots. Read the [Basic authentication tutorial](/cloud/confluence/basic-auth-for-rest-apis). 
- **Atlassian Connect JWT authentication:** Atlassian Connect JWT authentication allows you to act as the Atlassian Connect user in the system using JSON Web Tokens (JWT) as a standard way of representing security claims between the app and the Atlassian host product. Read the [JWT documentation](/cloud/confluence/understanding-jwt/). 
- **Atlassian Connect OAuth 2 JWT token exchange:** Atlassian Connect OAuth 2 JWT token exchange allows apps with the appropriate scope (ACT_AS_USER) to act as a user and request resources and perform actions in Jira and Confluence. <br>Read the [OAuth 2 JWT bearer token documentation](/cloud/confluence/oauth-2-jwt-bearer-tokens-for-apps/).

## Authorization

Authorization is the process of allowing your app or integration to take certain actions in the Atlassian application, after it has been authenticated. Confluence Cloud has different types of authorization, depending on whether you are building an Atlassian Connect app or calling the Confluence REST APIs directly.

### Authorization for apps

Atlassian Connect provides two types of authorization: 

*   **Authorization via scopes and app users:** This is the default authorization method. You should use this for most cases.
*   **Authorization via JWT bearer token authorization grant type for OAuth 2.0:** You should only use this method when your app needs to make server-to-server requests on behalf of a user.

#### Authorization for apps via scopes and app users

This method has two levels of authorization: static authorization via scopes and run-time authorization via app users. 

- **Scopes:* Scopes are defined in the app descriptor and statically specify the maximum set of actions that an app may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by app implementations. To learn more, read our page on [scopes](../scopes/). 
- **App users:**) Every app is assigned its own user in a Cloud instance. In general, server-to-server requests are made by the app user. This allows an administrator to restrict an app's access to certain projects and issues, just as they would for any other user. To learn more, read our [architecture overview](/cloud/jira/platform/architecture-overview/). Note that it is possible to make a request on behalf of the current user, rather than the app user. To do this, the request needs to be a client-side REST API request, using the [AP.request() method](../jsapi/request/), or the app needs to make a server-to-server request using an OAuth 2.0 bearer token (see next section). 

{{% tip %}}
**Combining static and run-time authorization**

The set of actions that an app is capable of performing is the intersection of the static scopes and the permissions of the user assigned to the request. This means that requests can be rejected because the assigned user lacks the required permissions. Therefore, your app should always defensively detect HTTP 403 forbidden responses from the product.
{{% /tip %}}

#### Authorization for apps via JWT bearer token authorization grant type for OAuth 2.0

At a high level, this method works by the app exchanging a JWT for an OAuth 2.0 access token (provided by the application). The access token can be used to make server-to-server calls, on behalf of the user, to the application's API.

To learn more, read our page on [OAuth 2.0 - JWT Bearer token authorization grant type](/cloud/confluence/oauth-2-jwt-bearer-tokens-for-apps).  