---
title: "Cacheable app iframes"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
date: "2018-08-21"
---

{{< include path="docs/content/cloud/connect/guides/cacheable-app-iframes.snippet.md" >}}
