---
title: "Context parameters"
platform: cloud
product: confcloud
category: devguide
subcategory: blocks
date: "2018-09-28"
---

# Context parameters

Context parameters are additional values pairs that are sent to your app in the request URL from
the host application. Using context parameters, your apps can selectively alter their behavior
based on information provided by the application. Common examples include displaying alternate
content, or even performing entirely different operations based on the available context.

## <a name="standard-parameters"></a>Standard parameters

The following parameters are common across all requests and are always included in the URL query
string.

* `lic`: the [license status](/platform/marketplace/cloud-app-licensing/)
* `loc`: the user's locale (eg: `en-GB`) (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
* `tz`: the user's timezone (eg: `Australia/Sydney`) (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
* `cp`: the context path of the instance (eg: `/wiki`)
* `xdm_e`: the base URL of the host application, used for the JavaScript bridge (xdm - cross domain messaging)
* `xdm_c`: the xdm channel to establish communication over
* `user_id`: the user's username. This may change and `user_key` should be used instead (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
* `user_key`: the user's unique identifier (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
* `cv`: the version of Atlassian Connect that made the HTTP request to your app.

{{% note %}}The `user_id` and `user_key` parameters are deprecated in favor of the `context.user`
JWT claim.{{% /note %}}

## Additional parameters

Your Atlassian Connect app can also request context parameters from the Atlassian application by
using variable tokens in the URL attributes of your modules. The app can use this to present
content specific to the context, for example, for the particular Jira issue or project that is open
in the browser. A particular variable is available only where it makes sense given the application
context.

URL variables are available to any of the page modules, including web panels, web items, general
pages and dialog pages, except for Confluence macros. To add a variable to a URL, enclose the
variable name in curly brackets, as follows: `{variable.name}`. The context variable must be either
a path component or the `value` in a query string parameter formatted as `name=value`.

For example, the following URL includes variables that are bound to the Jira project id and current
issue key at runtime:

``` json
{
    "url": "/myPage/projects/{project.id}?issueKey={issue.key}"
}
```

Note that conventional URL encoding means that context parameters passed as a query parameter will
be encoded slightly differently from those included as a path component. The path component will
use [percent encoding](https://en.wikipedia.org/wiki/Percent-encoding), while the query component
will use [`application/x-www-form-urlencoded` encoding](http://www.w3.org/TR/html5/forms.html#application/x-www-form-urlencoded-encoding-algorithm).
The primary difference to be aware of is that a space in a query parameter will be encoded as a
`+`, while in the path component it will be encoded as `%20`.

If the application isn't able to bind a value to the variable at runtime for any reason, it passes
an empty value instead.

## Inline conditions

[Conditions](/cloud/confluence/conditions) are a powerful way to control whether your UI elements
should be shown or not. However, sometimes you may want your element to always be shown but behave
differently depending on user permissions or other state that the conditions allow you to check.

You can achieve this goal by putting the conditions in query parameters. The condition will be
dynamically evaluated every time the URL is requested based on the current user and context.

The syntax of such variables is as follows (in the BNF notation with optional items enclosed in
square brackets):

```
<variable>        ::=  "condition." <condition-name> [<parameters>]
<parameters>      ::= "(" [<parameters-list>] ")"
<parameters-list> ::=  <parameter-key> "=" <parameter-value> | <parameters-list> "," <parameters-list>
```

Any condition available in other places can be used as a `condition-name`; `parameter-key` and
`parameter-value` are expected to be simple alphanumeric strings with some additional characters
like underscores or hyphens.

For example:

``` json
{
    "url": "/myPage/?adminMode={condition.is_admin_mode}",  
    "url2": "/myPage/?adminMode={condition.has_project_permission(permission=BROWSE_PROJECTS)}"
}
```

The following conditions do not work as context parameters:

* `user_is_the_logged_in_user` (Jira)
* `following_target_user` (Confluence)
* `tiny_url_supported` (Confluence)
* `viewing_content` (Confluence)
* `viewing_own_profile` (Confluence)

## Jira

Jira supports these context variables.

 * `user.id`, `user.key` (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
 * `issue.id`, `issue.key`, `issuetype.id`
 * `project.id`, `project.key`
 * `version.id`
 * `component.id`
 * `profileUser.accountId` (added by GDPR API Migration)
 * `profileUser.name`, `profileUser.key` (available for user profile pages) (marked for deprecation) (removed by [GDPR API Migration](../connect-app-migration-guide/))
 * `dashboardItem.id`, `dashboardItem.key`, `dashboardItem.viewType`, `dashboard.id` (available for dashboard items)

Jira issue pages only expose issue and project data. Similarly, version and component information
is available only in project administration pages.

#### Jira Service Desk

Jira Service Desk supports these context variables.

 * `servicedesk.requestId` for 'Request details view'
 * `servicedesk.requestTypeId` for 'Create request view' and 'Request details view'
 * `servicedesk.serviceDeskId` for 'Create request view', 'Request details view', and 'Portal page'

#### <a name="additional-parameters-jira-software"></a>Jira Software

Jira Software supports these context variables.

 * `board.id`, `board.type`, `board.screen` (for extension points displayed in multiple board screens),
 `board.screen` (formerly `board.mode`)
 * `sprint.id`, `sprint.state`

## Confluence

Confluence supports these context variables.

 * `user.id`, `user.key` (*DEPRECATED*) (removed by [GDPR API Migration](../connect-app-migration-guide/))
 * `content.id`, `content.version`, `content.type`, `content.plugin`
 * `space.id`, `space.key`
 * `page.id`, `page.version`, `page.type` <span>DEPRECATED</span>

Confluence provides the `content.*` variables wherever a page, blog post, or custom content is
present. Some examples are viewing or editing a page / blog post, or viewing a Confluence Question.

{{% note %}}The `page.*` variables are available, but have been deprecated in favor of their
`content.*` counterparts.{{% /note %}}

The `content.plugin` variable is a special case variable that is only present if the content in
question is a Custom Content entity provided by an app. For example, a question in Confluence
Questions will have a `content.plugin` value of
"com.atlassian.confluence.plugins.confluence-questions:question". As a general rule, the
`content.plugin` variable will only be present if `content.type` is equal to "custom".

## Apps

Apps can define their own context for [pages](https://developer.atlassian.com/cloud/jira/platform/modules/page/).
To do this, add query parameters to the URL of the Atlassian application. Use `ac.` as the prefix
for each query parameter key.

For example:

``` json
{
    "url": "/things/{ac.action}/?thingId={ac.thingId}"
}
```

In the URL above, `{ac.action}` and `{ac.thingId}` will be substituted with values of the
`ac.action` and `ac.thingId` parameters that are taken from the Atlassian application's current
URL. For example, if the application's current URL is
`http://example.atlassian.net/plugins/servlet/ac/add-on-key/general-page-key?ac.action=edit&ac.thingId=12`
then the resulting app URL will be `/things/edit/?thingId=12`.

If there are multiple query parameters with the same name in the Atlassian application URL, only
the value of the first occurrence will be used.
