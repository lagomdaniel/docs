---
title: "Major changes to Atlassian Connect APIs are coming to improve user privacy"
platform: cloud
product: jswcloud
category: devguide
subcategory: updates
date: "2018-06-12"
---

{{< reuse-page path="docs/content/cloud/jira/platform/api-changes-for-user-privacy-announcement-connect.md">}}