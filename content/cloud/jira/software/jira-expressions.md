---
title: "Jira expressions"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2019-04-12"
---
{{< include path="docs/content/cloud/connect/concepts/jira-expressions.snippet.md">}}