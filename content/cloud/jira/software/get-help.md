---
title: Resources 
platform: cloud
product: jswcloud
category: help
subcategory: help
layout: get-help
---

If you are looking for status updates for Atlassian products, no need to file a ticket. Just check
out the latest notifications and status in the links below.
