---
title: "Deprecation notice - representation of Request Language in Get Issue response"
platform: cloud
product: jsdcloud
category: devguide
subcategory: updates
date: "2019-02-13"
---

# Deprecation notice - representation of Request Language in Get Issue response

**From 18th August 2019**, Jira REST API responses that include the **Request Language** custom field type will no longer return a single translated string representing the body of the field. This data will still be available as part of a new JSON object representation wherever a string was previously expected.

### Replacement

After this date, you will only be able to get data for the **Request Language** custom field through its new JSON representation. 
The new JSON representation includes both the old value and a language key that is not dependant on the current user's language settings.
The new shape is available today via the get issue method with expanded `versionedRepresentations` in the 
[Jira Platform REST API](/cloud/jira/platform/rest/v3/#api-rest-api-3-issue-issueIdOrKey-get).

### Why?

A single string which is translated to the current user's language is not as useful as an object which also contains a standard language code that is the same regardless of the current user's language settings.

#### Feature parity

The new JSON object contains the old translated string, so this change has feature parity.

### Example

#### Format to be deprecated:

**Get Issue call:**

``` bash
https://your-domain.atlassian.net/rest/api/2/issue/DEMO-1?fields=customfield_11459
```

**GET Issue response:**

For a user who has their language set to "français (France)"

``` js
"customfield_11459": "anglais"
```

For a user who has their language set to "English (US)"

``` js
"customfield_11459": "English"
```

#### New version: 

**Get Issue call:**

``` bash
https://your-domain.atlassian.net/rest/api/2/issue/DEMO-1?fields=customfield_11459
```

**GET Issue response:**

For a user who has their language set to "français (France)"

``` js
"customfield_11459": {
    languageCode: "en",
    displayName: "anglais"
}
```

For a user who has their language set to "English (US)"

``` js
"customfield_11459": {
    languageCode: "en",
    displayName: "English"
}
```

#### Both versions:
As of today, the field data may be fetched in both old and new formats by using the `versionedRepresentations` expand.

**Get Issue call:**

``` bash
https://your-domain.atlassian.net/rest/api/2/issue/DEMO-1?fields=customfield_11459&expand=versionedRepresentations
```

**GET Issue response:**

``` js
"customfield_11459": {
    "1": "English"
    "2": {
        languageCode: "en",
        displayName: "English"
    }
}
```
