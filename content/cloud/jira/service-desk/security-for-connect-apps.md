---
title: "Security for Connect apps"
platform: cloud
product: jsdcloud
category: devguide
subcategory: securityconnect
date: "2018-12-17"
---
{{< include path="docs/content/cloud/connect/concepts/security-for-connect-apps.snippet.md">}}