---
title: Understanding JWT for Connect apps
platform: cloud
product: jsdcloud
category: devguide
subcategory: securityconnect
date: "2017-12-17"
---
{{< include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}