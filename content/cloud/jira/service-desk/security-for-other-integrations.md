---
title: "Security for other integrations"
platform: cloud
product: jsdcloud
category: devguide
subcategory: securityother
date: "2018-12-17"
---
{{< include path="docs/content/cloud/connect/concepts/security-for-other-integrations.snippet.md">}}