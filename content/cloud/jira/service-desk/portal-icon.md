---
title: Portal icon 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-service-desk-modules-portal-icon-41229857.html
- /jiracloud/jira-service-desk-modules-portal-icon-41229857.md
confluence_id: 41229857
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41229857
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41229857
date: "2017-08-25"
---
# Portal icon

The portal icon module is an icon that will be rendered next to the action. If the URL is relative it will be resolved against app's `baseUrl`. If none provided a default one will be rendered. Icon should follow this guideline: monochromatic with `#333333` color and transparent background, 16 x 16 pixel in size.

#### Sample JSON

``` json
{
  ...
  "icon": {
    "url": "/img/send_mail.png"
  }
}
```
#### Properties

`url`

-   **Type**: `string`
-   **Required**: yes
-   **Description**: Path to the icon image. If path is relative, it will be resolved against app's base URL.