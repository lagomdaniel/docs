---
title: "Latest updates for Jira Service Desk"
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
date: "2018-05-24"
---

# Latest updates

We deploy updates to Jira Service Desk Cloud frequently. As a Jira developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

{{% warning title="Important! API changes to improve user privacy"%}}
<p>On 24 May 2018 <a href="/cloud/jira/platform/api-changes-for-user-privacy-announcement">we announced upcoming API changes</a> to Jira Cloud REST APIs, webhooks, and extension points to support user privacy. Before continuing, please ensure you have read the <a href="/cloud/jira/platform/api-changes-for-user-privacy-announcement">change notice</a>.</p>{{% /warning %}}

## Recent changes

#### Updated SLA REST API

We are in the process of rolling out some changes to the SLA API (`servicedeskapi/request/{issueIdOrKey}/sla`). The `friendly` value inside of the `remainingTime`, `goalDuration` and `elapsedTime` objects will now use additional time units - days ("d"), weeks ("w") and years ("y").

#### Request create property panel

We have added support for defining your own [request create property panels](request-create-property-panel) which
are displayed on the request creation screen in the customer portal and enable apps to save arbitrary data during
request creation as Jira issue properties.

#### Issue fields support

Single and multi select [issue fields](modules/issue-field) supplied by Connect apps are now supported in JSD customer portal.

#### Non-experimental APIs

We have removed the experimental flag from the following APIs:

* `/servicedeskapi/customer`
* `/servicedeskapi/servicedesk/{serviceDeskId}/customer` (POST only)
* `/servicedeskapi/request/{issueIdOrKey}/approval`
* `/servicedeskapi/request/{issueIdOrKey}/approval/{approvalId}`

#### Updated REST APIs

We updated all of our REST APIs to support [next-gen](https://blog.developer.atlassian.com/next-gen-projects-jira-cloud/) projects except the following which will return appropriate not supported in next-gen error messages: 

* POST `/servicedeskapi/servicedesk/{serviceDeskId}/requesttype`
* GET `/servicedeskapi/request/{issueIdOrKey}/transition`
* POST `/servicedeskapi/request/{issueIdOrKey}/transition`
* GET `/servicedeskapi/request/{issueIdOrKey}/approval`
* GET `/servicedeskapi/request/{issueIdOrKey}/approval/{approvalId}`
* POST `/servicedeskapi/request/{issueIdOrKey}/approval/{approvalId}`

We have made the following changes to the REST APIs:

* Added the `searchQuery` parameter to `/servicedeskapi/requesttype`
* Added the `approvalStatus` parameter and the `requestOwnership=APPROVER` value to `/servicedeskapi/request`
* Added `/servicedeskapi/request/{issueIdOrKey}/notification`

For up-to-date and complete docs, see the [JSD REST API documentation](/cloud/jira/service-desk/rest/).

#### Deprecation notices

- [Deprecation notice - Request Language custom field JSON representation in Get Issue response](deprecation-notice-representation-of-request-language-in-get-issue-response)

#### Platform
Also see the [latest updates for the Jira Platform](../platform).

## Atlassian Developer blog

Major changes that affect Jira Cloud developers are announced in the **Atlassian Developer blog**, like new Jira modules or the deprecation of API end points.
You'll also find handy tips and articles related to Jira development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/)
 ([Jira-related posts](https://developer.atlassian.com/blog/categories/jira/))

## Release notes

Changes to Jira Service Desk Cloud are announced on the Jira Service Desk blog.

Changes that affect all Jira Cloud products are announced in the *What's New blog* for Atlassian Cloud. This includes new features, bug fixes, and other changes. For example, the introduction of a new Jira quick search or a change in project navigation.

Check them out and subscribe here:
[Jira Service Desk blog](https://confluence.atlassian.com/pages/viewrecentblogposts.action?key=SERVICEDESKCLOUD)
[What's new blog](https://confluence.atlassian.com/servicedeskcloud/blog) (Note, this blog also includes changes to other Cloud applications)
