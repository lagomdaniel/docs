---
title: "Cookie-based auth for REST APIs"
platform: cloud
product: jsdcloud
category: devguide
subcategory: securityother
date: "2018-12-17"
---
{{< reuse-page path="docs/content/cloud/jira/platform/jira-rest-api-cookie-based-authentication.md">}}