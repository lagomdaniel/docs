---
title: "Understanding JWT for Connect apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityconnect
aliases:
- /cloud/jira/platform/understanding-jwt.html
- /cloud/jira/platform/understanding-jwt.md
date: "2018-06-15"
---

{{< include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}
