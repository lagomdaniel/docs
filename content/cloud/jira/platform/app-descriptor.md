---
aliases:
- /jiracloud/add-on-descriptor.html
- /jiracloud/add-on-descriptor.md
category: devguide
platform: cloud
product: jiracloud
subcategory: blocks
title: "App descriptor"
date: "2017-08-24"
---

{{< include path="docs/content/cloud/jira/platform/temp/app-descriptor.snippet.md" >}}

{{< include path="docs/content/cloud/connect/reference/descriptor-schemas.snippet.md">}}

{{< include path="docs/content/cloud/jira/platform/temp/app-descriptor-reference.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/authentication.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/lifecycle.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/app-vendor.snippet.md" >}}