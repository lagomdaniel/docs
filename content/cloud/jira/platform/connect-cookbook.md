---
title: "Connect cookbook"
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2017-10-24"
---
# Connect cookbook

This cookbook is an extension of the [JavaScript API Cookbook](../about-the-javascript-api/#cookbook),
which provides snippets of code that can be used in the clients browser to get information from the product.  

## Access a list of Jira projects

Use this to retrieve a list of your Jira projects. Depending on your projects, you might need to paginate to see 
complete results. You can do this by passing  `startAt` in the request query string.


``` javascript
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/project',
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }  
  });
});
```

## Search for an issue using JQL

In this example you'll create a simple 
[JQL (Jira query language)](https://confluence.atlassian.com/x/awiiLQ) 
query that looks for unresolved issues (`resolution = null`). The JQL query is in the `searchJql` parameter of the
request. You might need to paginate your results to get through all of them.


``` javascript
var searchJql = 'resolution = null';
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/search?jql=' + encodeURIComponent(searchJql),
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    }    
  });
});
```

## Create a Jira issue

This recipe creates a new issue for an existing Jira project. Depending on how
your project is configured, you might need to include additional fields. See
[Create
issue](https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-rest-api-3-issue-post)
in the REST API documentation for details.


``` javascript
var issueData = {
  "fields": {
    "project": { 
      "key": "TEST"
    },
    "summary": "REST ye merry gentlemen.",
    "description": "Creating of an issue using project keys and issue type names using the REST API",
    "issuetype": {
      "name": "Task"
    }
  }
};
AP.require('request', function(request) {
  request({
    url: '/rest/api/latest/issue',
    // adjust to a POST instead of a GET
    type: 'POST',
    data: JSON.stringify(issueData),
    success: function(response) {
      // convert the string response to JSON
      response = JSON.parse(response);

      // dump out the response to the console
      console.log(response);
    },
    error: function() {
      console.log(arguments);
    },
    // inform the server what type of data is in the body of the HTTP POST
    contentType: "application/json"    
  });
});
```