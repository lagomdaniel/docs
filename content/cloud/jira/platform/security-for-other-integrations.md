---
title: "Security for other integrations"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityother
aliases:
- /jiracloud/security-for-other-integrations.html
- /jiracloud/security-for-other-integrations.md
date: "2018-12-17"
---
{{< include path="docs/content/cloud/connect/concepts/security-for-other-integrations.snippet.md">}}