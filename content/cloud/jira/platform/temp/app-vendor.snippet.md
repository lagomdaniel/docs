## App vendor

Gives basic information about the app vendor

``` json
{
  "vendor": {
    "name": "Atlassian",
    "url": "http://www.atlassian.com"
  }
}
```

### Properties

<table summary="App vendor properties">
  <tbody>
    <tr>
      <td><code>name</code></td>
      <td><p><strong>Type:</strong> <code>string</code></p>
        <p><strong>Description:</strong> The name of the app vendor. Supply your name or the name of the company you work for.</p>
        </td>
    </tr>
    <tr>
      <td><code>url</code></td>
      <td><p><strong>Type:</strong> <code>string</code> (<code>uri</code>)</p>
        <p><strong>Description:</strong> The URL for the vendor's website.</p>
      </td>
    </tr>
  </tbody>
</table>