## Lifecycle

Allows an app to register callbacks for events that occur in the lifecycle of an installation. When a lifecycle event is fired, a POST request will be made to the appropriate URL registered for the event.

The installed lifecycle callback is an integral part of the installation process of an app, whereas the remaining lifecycle events are essentially webhooks.

Each property in this object is a URL relative to the app's base URL.

###  Lifecycle attribute example
``` json
{
  "installed": "/installed",
  "uninstalled": "/uninstalled",
  "enabled": "/enabled",
  "disabled": "/disabled"
}
```
### Lifecycle HTTP request payload
Lifecycle callbacks contain a JSON data payload with important tenant information that you will need to store in your app in order to sign and verify future requests. The payload contains the following attributes:

``` json
{
  "key": "installed-addon-key",
  "clientKey": "unique-client-identifier",
  "sharedSecret": "a-secret-key-not-to-be-lost",
  "serverVersion": "server-version",
  "pluginsVersion": "version-of-connect",
  "baseUrl": "http://example.atlassian.net",
  "productType": "jira",
  "description": "Atlassian Jira at https://example.atlassian.net",
  "serviceEntitlementNumber": "SEN-number",
  "eventType": "installed"
}
```

<table summary="Lifecycle payload attributes" class="aui">
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody><tr>
        <td><code>key</code></td>
        <td>App key that was installed into the Atlassian Product, as it appears in your app's descriptor.</td>
    </tr>
    <tr>
        <td><code>clientKey</code></td>
        <td>Identifying key for the Atlassian product instance that the app was installed into. This will never change for a given
        instance, and is unique across all Atlassian product tenants. This value should be used to key tenant details
        in your app. The one time the clientKey can change is when a backup taken from a different instance is restored onto the instance.
        Determining the contract between the instance and app in this situation is tracked by
        <a href="https://ecosystem.atlassian.net/browse/AC-1528">AC-1528</a> in the Connect backlog.</td>
    </tr>
    <tr>
        <td><code>publicKey</code></td>
        <td><span class="aui-lozenge aui-lozenge-current">Deprecated</span> This is the public key for this Atlassian
        product instance. This field is <a href="https://developer.atlassian.com/static/connect/docs/latest/release-notes/deprecations.html">deprecated</a> and should not be
        used.</td>
    </tr>
    <tr>
        <td><code>sharedSecret</code></td>
        <td>Use this string to sign outgoing JWT tokens and validate incoming JWT tokens. Optional: and may not
        be present on non-JWT app installations, and is only sent on the <code>installed</code> event.</td>
    </tr>
    <tr>
        <td><code>serverVersion</code></td>
        <td>This is a string representation of the host product's version. Generally you should not need it.</td>
    </tr>
    <tr>
        <td><code>pluginsVersion</code></td>
        <td>This is a semver compliant version of Atlassian Connect which is running on the host server, for example: <code>1.1.15</code>.</td>
    </tr>
    <tr>
        <td><code>baseUrl</code></td>
        <td>URL prefix for this Atlassian product instance. All of its REST endpoints begin with this `baseUrl`.</td>
    </tr>
    <tr>
        <td><code>productType</code></td>
        <td>Identifies the category of Atlassian product, e.g. <code>jira</code> or <code>confluence</code>.</td>
    </tr>
    <tr>
        <td><code>description</code></td>
        <td>The host product description - this is customisable by an instance administrator.</td>
    </tr>
    <tr>
        <td><code>serviceEntitlementNumber</code>
        (optional)</td>
        <td>Also known as the SEN, the service entitlement number is the app license ID. This attribute will only be included
        during installation of a paid app.</td>
    </tr>
      <tr>
        <td><code>oauthClientId</code></td>
        <td>The OAuth 2.0 client ID for your app. For more information, see <a href="https://developer.atlassian.com/cloud/jira/platform/oauth-2-jwt-bearer-token-authorization-grant-type/">OAuth 2.0 - JWT Bearer token authorization grant type</a> </td>
    </tr>
</tbody></table>