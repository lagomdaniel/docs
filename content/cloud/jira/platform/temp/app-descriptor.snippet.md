# App descriptor

The app descriptor is a JSON file (`atlassian-connect.json`) that describes the app to the Atlassian application. The descriptor includes general information for the app, as well as the modules that the app wants to use or extend.

If you're familiar with Java app development with previous versions of the Atlassian Plugin Framework, you may already be familiar with the `atlassian-plugin.xml` descriptors. The `atlassian-connect.json` serves the same function.

The descriptor serves as the glue between the remote app and the Atlassian application. When an administrator for a cloud instance installs an app, what they are really doing is installing this descriptor file, which contains pointers to your service. You can see an example below.

For details and application-specific reference information on the descriptor please refer to the [Jira modules](/cloud/jira/platform/about-jira-modules) and [Confluence modules](/cloud/confluence/modules/dialog/) sections of this documentation. But we'll call out a few highlights from the example here.

Since Atlassian Connect apps are remote and largely independent from the Atlassian application, they can be changed at any time, without having to create a new version or report the change to the Atlassian instance. The changes are reflected in the Atlassian instance immediately (or at least at page reload time).

However, some app changes require a change to the descriptor file itself. For example, an app could be modified to have a new page module. Since this requires a page module declaration in the descriptor, it means making an updated descriptor available. Until that updated descriptor is re-installed into the Atlassian Product that change in the descriptor file will not take effect. To propagate a change in the descriptor to the Atlassian products, you need to create a new version of the app in its Marketplace listing. The Marketplace will take care of the rest: informing administrators and automatically installing the available update. See [Upgrades](/platform/marketplace/upgrading-and-versioning-cloud-apps/) for more details.