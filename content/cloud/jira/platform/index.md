---
title: "Latest updates"
platform: cloud
product: jiracloud
category: devguide
subcategory: index
aliases:
- /jiracloud/latest-updates-39988013.html
- /jiracloud/latest-updates-39988013.md
confluence_id: 39988013
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988013
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988013
date: "2018-10-01"
---

# Latest updates

We deploy updates to Jira Cloud frequently. As a Jira developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

{{% warning %}}

#### Important! Deprecation notice for API changes to improve user privacy

<br>On 01 October 2018, we announced that several Jira Cloud REST APIs are being deprecated in order to improve user privacy.
Before continuing, please read the <a href="/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/">deprecation notice and migration guide</a>.{{% /warning %}}

## Recent announcements

Changes announced in the Atlassian Developer blog are usually described in more detail in this documentation. The most recent announcements are documented in detail below:

- [Change notice - GET search optimized for fields id and key](/cloud/jira/platform/change-notice-for-get-search-max-results/)
- [Deprecation notice - Removal of non paginated project and filter endpoints](/cloud/jira/platform/deprecation-notice-removal-of-get-filters-and-get-all-projects/)
- [Change notice - Required encoding of some characters used in REST API calls](/cloud/jira/platform/change-notice-required-encoding-characters-for-rest-api-calls/)
- [Deprecation notice and migration guide for major changes to Jira Cloud REST APIs to improve user privacy](/cloud/jira/platform/deprecation-notice-user-privacy-api-migration-guide/)
- [Deprecation notice - Registering webhooks with non-secure URLs](/cloud/jira/platform/deprecation-notice-registering-webhooks-with-non-secure-urls)
- [Change notice - Get my permissions resource now requires the permissions query parameter](/cloud/jira/platform/change-notice-get-my-permissions-requires-permissions-query-parameter)
- [Major changes to Atlassian Connect APIs are coming to improve user privacy](/cloud/jira/platform/api-changes-for-user-privacy-announcement-connect)
- [Major changes to Jira Cloud REST APIs are coming to improve user privacy](/cloud/jira/platform/api-changes-for-user-privacy-announcement)
- [Change notice - Location will become a required field in board creation sdfs](/cloud/jira/platform/change-notice-location-becomes-required-field)
- [Deprecation notice - Basic authentication with passwords and cookie-based authentication](/cloud/jira/platform/deprecation-notice-basic-auth-and-cookie-based-auth)
-	[Deprecation notice - removal of web fragment locations in new Jira Cloud experience](/cloud/jira/platform/deprecation-notice-removal-of-tab-panels)
-   [Deprecation notice - toString representation of sprints in Get issue response](/cloud/jira/platform/deprecation-notice-tostring-representation-of-sprints-in-get-issue-response/)

## Jira Cloud Platform API roadmap Trello board

We maintain a board on Trello to communicate the current high-level priorities for API improvements in Jira Cloud. It's also the best place to see what has shipped recently. [Check out the board](https://trello.com/b/cI3iT8jv/jira-cloud-platform-api-roadmap).

## How do I find out about user-facing changes in Jira Cloud?

Major changes that affect all users of the Jira Cloud products are announced in the [*What's New blog* for Atlassian Cloud](https://confluence.atlassian.com/cloud/blog). This includes new features, bug fixes, and other changes. For example, the introduction of a new Jira quick search or a change in project navigation.

Check it out and subscribe here: [What's new blog](https://confluence.atlassian.com/display/Cloud/What%27s+New) *(Note, this blog also includes changes to other Cloud applications)*

## Atlassian Developer blog

Major changes that affect Jira Cloud developers are also announced in the *Atlassian Developer blog*, like new Jira modules or the deprecation of API end points. You'll also find handy tips and articles related to Jira development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/) *(Jira-related posts)*
