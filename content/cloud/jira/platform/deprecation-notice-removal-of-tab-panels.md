---
title: "Deprecation notice - removal of web fragment locations in new Jira Cloud experience"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2017-10-10"
---

# Deprecation notice - removal of project tab panels, profile tab panels, and project admin summary panels

## Deprecation notice date: 10 October 2017

As we roll out the [new Jira Cloud experience], several locations for providing content in the Jira Cloud user interface have evolved and been replaced with new APIs.

### Project tab panel and profile tab panels

The `jiraProjectTabPanels` and `jiraProfileTabPanels` [modules](/cloud/jira/software/modules/tab-panel/) have been replaced with more modern alternatives.

#### Replacement

The initial location for `jiraProjectTabPanels` was phased out of Jira Cloud in March 2015. We recommend that apps implement web items and web panels in the [Jira project sidebar] instead.

We will remove support for `jiraProfileTabPanels` in Jira Cloud soon. If your app has user-specific content, we recommend you create a [general page] and a link to this page from the [user profile menu].

### Project admin summary panels

We will remove the project admin summary page from Jira Cloud soon. Locations that were previously available on this page included:

* `webpanels.admin.summary.left-panels`
* `webpanels.admin.summary.right-panels`

#### Replacement

No replacement will be provided.

Report feature requests and bugs for Jira Cloud and webhooks in the [ACJIRA project](https://ecosystem.atlassian.net/projects/ACJIRA) on ecosystem.atlassian.net.

[new Jira Cloud experience]: https://confluence.atlassian.com/display/JIRASOFTWARECLOUD/Introducing+your+new+Jira+experience
[Jira project sidebar]: /cloud/jira/platform/jira-project-sidebar
[general page]: /cloud/jira/software/modules/page
[user profile menu]: /cloud/jira/platform/user-profile-menu