## Location-based modules

These modules provide content in a specific location in the Jira Cloud UI, as defined 
in your [app descriptor]. Within the Atlassian developer community, these module types 
are commonly referred to as _web fragments_. They include:

* [Web item]: displays a link or button. These can be used to _target_ (open) other modules like pages or dialogs
* [Web section]: provides a location to group several web items together
* [Web panel]: display content in a specific context with Jira Cloud (like in a panel on the issue view page)

### Available locations for location-based modules
Locations for web items, web sections, and web panels are available across Jira Cloud. 
Consult the linked pages below for a comprehensive list of locations available on each 
page in Jira Cloud. **There may be more valid locations that aren't documented below, 
but we don't guarantee that we support them.**

* [Issue view]
* [Project sidebar]
* [Jira administration (global settings)]
* [User profile menu]
* [Jira home container (global navigation)]
* [Jira project settings]
* [Jira Software boards]

## New Jira issue view modules

We're currently rolling out a [new issue view for Jira Cloud], with two new extension patterns: [quick-add buttons], to add content to help describe issues, and [glances], which let users include and show contextual information from your app on issues.
You can read more about these patterns on our [new issue view UI locations page], and take a look at the [design guidelines] to see how you can increase your user base and provide a great user experience.

## Pages and dialogs

Jira Cloud provides support for [pages], which display content on a full screen page. 
There are several types of page modules. General pages are added to the Jira global 
sidebar, but there are other types that are available in specific locations. [Review 
the documentation] to see the various types and their uses. 

Jira also provides a [dialog] module type that can be opened either by a user 
clicking a button you provide or inside your app's iframe via the [`Dialog` JavaScript 
API module].

## Advanced UI modules

Jira Cloud also provides specific module types for certain types of UI content that extend specific Jira features. These include:

* [Issue tab panel]: provides a location in the tabbed content section of the issue view (where comments on an issue are located)
* [Dashboard items]: allows apps to provide a gadget to display on Jira Cloud dashboards
* [Project admin tab panels]: provides a new page in the project settings area of Jira
* [Report]: provides a page type that will be rendered in Jira Cloud's project reports page
* [Search request view]: renders a custom view of a search result that's accessible from the Jira issue navigator
* [Workflow post-functions]: this module contains both a webhook for apps to be notified when an issue transition occurs, and a location to provide a configuration screen for the user

### Jira Software- and Jira Service Desk-specific locations
Jira Software also provides locations for web items and web panels on [boards], and 
Jira Service Desk provides several dedicated module types for integrating with the 
agent and customer views on tickets. You can learn more about these special module 
types in the [Jira Software Cloud] and [Jira Service Desk Cloud] modules pages.

## Non-UI modules

There are also a number of [Jira Cloud modules] that extend other concepts in Jira, 
where Jira will handle the rendering of your data. The most common of these are [Issue 
Fields], which allow your app to provide fields that will be treated as native Jira 
Cloud custom fields, and [Webhooks], which allow your app to be notified when events 
take place in Jira Cloud. There are also modules related to permissions, time 
tracking, and keyboard shortcuts. See the full Jira Cloud modules reference to learn 
more


[modules]: /cloud/jira/platform/about-jira-modules/
[Jira Cloud modules]: /cloud/jira/platform/about-jira-modules/
[app descriptor]: /cloud/jira/platform/app-descriptor
[Pages]: /cloud/jira/platform/modules/page
[Review the documentation]: /cloud/jira/platform/modules/page/
[Web panel]: /cloud/jira/platform/modules/web-panel
[Web item]: /cloud/jira/platform/modules/web-item
[Web section]: /cloud/jira/platform/modules/web-section
[dialog]: /cloud/jira/platform/modules/dialog/
[`Dialog` JavaScript API module]: /cloud/jira/platform/jsapi/dialog/
[Tab panel]: /cloud/jira/platform/modules/tab-panel
[`jiraProjectTabPanels`]: /cloud/jira/platform/modules/tab-panel
[Issue tab panel]: /cloud/jira/platform/modules/tab-panel
[Dashboard items]: /cloud/jira/platform/modules/dashboard-item
[Project admin tab panels]: /cloud/jira/platform/modules/project-admin-tab-panel
[Report]: /cloud/jira/platform/modules/report
[Search request view]: /cloud/jira/platform/modules/search-request-view
[Workflow post-functions]: /cloud/jira/platform/modules/workflow-post-function
[boards]: /cloud/jira/software/boards
[Jira Software boards]: /cloud/jira/software/boards
[Jira Software Cloud]: /cloud/jira/software/about-jira-modules
[Jira Service Desk Cloud]: /cloud/jira/service-desk/about-jira-modules
[Issue Fields]: /cloud/jira/platform/modules/issue-field/
[Project Page]: /cloud/jira/platform/modules/project-page/
[Webhooks]: /cloud/jira/platform/modules/webhook/
[Issue view]: /cloud/jira/platform/issue-view-ui-locations
[Project sidebar]: /cloud/jira/platform/jira-project-sidebar
[Jira administration (global settings)]: /cloud/jira/platform/administration-ui-locations
[User profile menu]: /cloud/jira/platform/user-profile-menu
[Jira home container (global navigation)]: /cloud/jira/platform/home-container/
[Jira project settings]: /cloud/jira/platform/modules/project-admin-tab-panel
[Jira Software boards]: /cloud/jira/software/boards/
[new issue view for Jira Cloud]: https://confluence.atlassian.com/display/JIRACORECLOUD/The+new+Jira+issue+view
[alpha program for app developers]: https://developer.atlassian.com/blog/2017/12/alpha-program-for-the-new-jira-issue-view/
[quick-add buttons]: /cloud/jira/platform/modules/issue-content/
[glances]: /cloud/jira/platform/modules/issue-glance/
[new issue view UI locations page]: /cloud/jira/platform/new-issue-view-ui-locations/
[design guidelines]: /cloud/jira/platform/issue-view/