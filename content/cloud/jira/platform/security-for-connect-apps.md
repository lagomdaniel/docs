---
title: "Security for Connect apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityconnect
aliases:
- /jiracloud/security-for-connect-apps.html
- /jiracloud/security-for-connect-apps.md
date: "2018-12-17"
---
{{< include path="docs/content/cloud/connect/concepts/security-for-connect-apps.snippet.md">}}