---
title: "User profile menu"
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.md
confluence_id: 39988374
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988374
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988374
date: "2017-09-28"
---
# User profile menu

The Jira Cloud user profile menu is the recommended location for any view your app 
provides that has a user context. This means that your app will display information or 
make actions available that are specific to the current user.

This location supports only `web items`.

### Available locations

For web items:

* `system.user.options/personal`
* `system.user.options/system`

![project navigation overview](../images/user-profile-menu.png)

### Sample descriptor JSON

``` json
"modules": {
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.user.options/personal",
            "name": {
                "value": "Example app link"
            },
            "url": "/example-section-link"
        }
    ]
}
```

### Properties

The properties required for this location are the standard ones defined in the documentation for [web items].

[web items]: /cloud/jira/platform/modules/web-item/
