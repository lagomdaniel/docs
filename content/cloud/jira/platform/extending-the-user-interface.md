---
title: "Extending the user interface"
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
date: "2017-09-29"
---
# Extending the Jira Cloud user interface

The most common (and most powerful) way for apps to extend Jira Cloud is by adding 
content directly to the Jira user interface. Jira Cloud provides a number of [modules](/cloud/jira/platform/about-jira-modules/) to provide different types of 
content. All Jira Cloud UI modules use iframes to sandbox content. There are three types 
of modules for apps to provide UI elements:

{{< include path="docs/content/cloud/jira/platform/modules.snippet.md">}}