---
title: "About Jira modules"
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-39987040.html
- /jiracloud/jira-platform-modules-39987040.md
confluence_id: 39987040
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987040
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987040
date: "2017-09-11"
---
# About Jira modules

Jira modules allow apps to extend the functionality of the Jira platform or a Jira application.
Jira modules are commonly used to extend the user interface by adding links, panels, or pages.
However, some Jira modules can also be used to extend other parts of Jira, like permissions and
workflows. Jira Service Desk and Jira Software also have their own application-specific modules.

## Using Jira modules

To use a Jira module, declare it in your app descriptor (under `modules`) with the
appropriate properties. For example, the following code adds the `generalPages` module at the `
system.top.navigation.bar` location to your app, which adds a link in the [Jira home container].

**atlassian-connect.json**

``` java
...
"modules": {
          "generalPages": [
              {
                  "key": "activity",
                  "location": "system.top.navigation.bar",
                  "name": {
                      "value": "Activity"
                  }
              }
          ]
      }
...
```

There are two types of modules: basic iframes that allow you to display content in different locations in Jira, and more advanced modules that let you provide advanced Jira-specific functionality.

{{< include path="docs/content/cloud/jira/platform/modules.snippet.md">}}

[Jira Software modules]: /cloud/jira/software/jira-software-modules
[Jira Service Desk modules]: /cloud/jira/service-desk/jira-service-desk-modules
[Dialog]: /cloud/jira/platform/modules/dialog
[Page]: /cloud/jira/platform/modules/page
[Web panel]: /cloud/jira/platform/modules/web-panel
[Web item]: /cloud/jira/platform/modules/web-item
[Web section]: /cloud/jira/platform/modules/web-section
[Webhook]: /cloud/jira/platform/modules/webhook
[Webhooks]: /cloud/jira/platform/webhooks
[Administration console]: /cloud/jira/platform/administration-ui-locations
[End-user locations]: /cloud/jira/platform/extension-points-for-the-end-user-ui
[Project configuration]: /cloud/jira/platform/project-settings-ui-locations
[Project sidebar]: https://developer.atlassian.com/jiradev/jira-platform/guides/projects/design-guide-jira-project-centric-view/development-guide-jira-project-centric-view
[View issue page]: /cloud/jira/platform/issue-view-ui-locations
[Dashboard item]: /cloud/jira/platform/modules/dashboard-item
[Entity property]: /cloud/jira/platform/modules/entity-property
[Global permission]: /cloud/jira/platform/modules/global-permission
[Project admin tab pane]: /cloud/jira/platform/modules/project-admin-tab-panel
[Project permission]: /cloud/jira/platform/modules/project-permission
[Report]: /cloud/jira/platform/modules/report
[Search request view]: /cloud/jira/platform/modules/search-request-view
[Tab panel]: /cloud/jira/platform/modules/tab-panel
[Workflow post-function]: /cloud/jira/platform/modules/workflow-post-function
[Jira home container]: /cloud/jira/platform/jira-home-container
