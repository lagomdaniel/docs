---
title: "Issue view UI locations"
platform: cloud
product: jiracloud
category: reference
subcategory: modules 
aliases:
- /jiracloud/jira-platform-modules-view-issue-page-39988380.html
- /jiracloud/jira-platform-modules-view-issue-page-39988380.md
confluence_id: 39988380
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988380
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988380
date: "2018-08-02"
---
# Issue view UI locations

The most common location to add content from your app is on Jira's *issue view*. There 
are two general types of locations for your content on the *issue view* page.

<div class="aui-message note">
    <div class="icon"></div>
    <p class="title"><strong>The new Jira issue view</strong></p><br>
    <p>We're currently rolling out a <a href="https://confluence.atlassian.com/display/JIRACORECLOUD/The+new+Jira+issue+view">new issue view for Jira Cloud</a> with new issue view extension points. Take a look at the <a href="/cloud/jira/platform/new-issue-view-ui-locations/">new issue view UI locations page</a> to find out more and get started with the new design.</p>
</div>

You can add `web items` as actions that the user takes on an issue. This is ideal for 
triggering [dialogs] provided by your app that allow users to take action on the issue, 
supported by your app. Actions are also great for linking the Jira issue to external 
content that may live outside of Jira, or even creating new content like a diagram that 
you then attach to the issue.

The other type of location is a panel, which is used to provide content from your app 
relevant to that issue. This might be external content, or a special visualization of 
content stored within Jira.

## Issue action locations

Issue actions, such as sharing an issue, exporting an issue, and more, are implemented 
via buttons at the top right of the issue view. You can extend issue actions either by 
adding a new web item next to the issue actions, or by inserting a new web section or 
web item in the more actions (•••) menu.

For `web items`:

* `jira.issue.tools`: Add a button in the issue actions area
* Ellipsis menu locations:
    * `operations-top-level`
    * `operations-work`
    * `operations-attachments`
    * `operations-voteswatchers`
    * `operations-subtasks`
    * `operations-operations`
    * `operations-delete`

![Jira issue actions locations](../images/jira-actions-locations.png)

### Sample descriptor JSON
``` json
...
"modules": {
    "webItems": [
        {
            "key": "example-tools-item",
            "location": "operations-operations",
            "weight": 10,
            "name": {
                "value": "Example app link"
            },
            "url": "/example-section-link"
        }
    ]
}
...
```

### Properties

The properties required for this location are the standard ones defined in the documentation for [web items].

## Panel content locations

You can also define web panels to render larger pieces of content on the *issue view* 
page. These panels use the [`web panel` module] and are available on either the right 
or left side of the page. We recommend that you follow Jira's standard convention, that 
information on the right hand side provides **context** for the issue (more 
information, links to relevant content, other work associated with the issue). Panels 
on the left side should provide **content** that you expect the user to consider part 
of the body of work that the issue itself represents.

### Available locations

For web panels:

* `atl.jira.view.issue.left.context`
* `atl.jira.view.issue.right.context`

![Jira issue view panel locations](../images/jira-view-issue-panels.png)

#### Sample JSON
``` json
...
"modules": {
    "webPanels": [
        {
            "key": "example-issue-left-panel",
            "location": "atl.jira.view.issue.left.context",
            "name": {
                "value": "App panel location left side title"
            },
            "url": "/example-issue-left-panel"            
        },
        {
            "key": "example-issue-right-panel",
            "location": "atl.jira.view.issue.right.context",
            "name": {
                "value": "App panel location right side title"
            },
            "url": "/example-issue-right-panel"            
        }
    ]   
}
...
```

### Properties

The properties required for this location are the standard ones defined in the 
documentation for [web panels](/cloud/jira/platform/modules/web-panel/).

[web sections]: /cloud/jira/platform/modules/web-section
[web items]: /cloud/jira/platform/modules/web-item/
[dialogs]: /cloud/jira/platform/modules/dialog/
[`web panel` module]: /cloud/jira/platform/modules/web-panel/