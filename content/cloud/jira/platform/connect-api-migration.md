---
title: "Connect API migration"
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2018-09-01"
aliases:
- /cloud/jira/platform/connect-api-migration.html
- /cloud/jira/platform/connect-api-migration.md
---
{{< include path="docs/content/cloud/connect/concepts/connect-api-migration.snippet.md">}}
