<a name="reportingapi"></a>

## Personal data reporting API reference

The _Personal data reporting API_ is a RESTful API that allows apps to report the user accounts 
for which they are storing personal data. For flexibility and efficiency, the API allows multiple 
accounts to be reported on in a single request.

### Report accounts API ###

`POST https://api.bitbucket.org/atlassian-connect/report-accounts`

Reports a list of user accounts and gets information on whether the personal data for each account 
needs to be updated or erased.

**Note:** In most places, Bitbucket lets you use Atlassian Account IDs and Bitbucket User UUIDs 
interchangably. However, to report personal data usage, you must use Atlassian Account IDs.

#### Authentication ####

The _Personal data reporting API_ supports two types of authentication, depending on whether or not
the app is a Bitbucket Connect app or a Bitbucket OAuth 2.0 authorization code grant (3LO) app:

* **Authentication for Connect apps**: Bitbucket Connect apps use JWT authentication to make API requests.
Please read about [authentication for apps](../authentication-for-apps/)
to learn how to authenticate for this API. You can use any valid shared secret when making a
request--it doesn't have to be one from one of the users which you are reporting.
* **Authentication for OAuth 2.0 authorization code grant (3LO) apps**: Oauth consumers must use a
[client credentials grant](../oauth-2/#4-client-credentials-grant-4-4)
to authenticate for this API as the owner of the consumer.

#### Parameters
This operation has no parameters.

#### Request
_Content type: application/json_<br>
Each request allows up to 90 accounts to be reported on. For each account, the 
accountId and time that the personal data was retrieved must be provided. The time format is 
defined by 
[RFC 3339, section 5.6](https://tools.ietf.org/html/rfc3339#section-5.6).<br>
Example request (application/json):

``` json
{
"accounts": [{
    "accountId": "account-id-a",
    "updatedAt": "2017-05-27T16:22:09.000Z"
  }, {
    "accountId": "account-id-b",
    "updatedAt": "2017-04-27T16:23:32.000Z"
  }, {
    "accountId": "account-id-c",
    "updatedAt": "2017-02-27T16:22:11.000Z"
  }]
}
```

#### Responses

- `200` (_Content type: application/json_): The request is successful and one or more personal data 
erasure actions are required. The information is contained in an `accounts` array where each object 
identifies the accountId and whether the reason for the erasure is due to the closure of 
the account or invalidation of the app's copy of personal data due to the some update. In 
the case of the latter, the app is permitted to re-request personal data.

  Example response (application/json):

  ``` json
  {
    "accounts": [{
      "accountId": "account-id-a",
      "status": "closed"
    }, {
      "accountId": "account-id-c",
      "status": "updated"
    }]
  }
  ```
- `204`: The request is successful and no action by the app is required, with respect to the accounts 
sent in the request.
- `400`: The request was malformed in some way. The response body contains an error message.

  Example response (application/json):

  ``` json
  {
    "errorType": "string",
    "errorMessage": "string"
  }
  ```
- `403`: The request is forbidden.
- `429`: Rate limiting applies. The app must follow the rate limiting directives provided in response 
headers.
- `500`: An internal server error occurred. The response body contains an error message.

  Example response (application/json):

  ``` json
  {
    "errorType": "string",
    "errorMessage": "string"
  }
  ```
- `503`: The service is currently unavailable. The service may be unavailable during initial development 
or due to an outage of an upstream dependency.
