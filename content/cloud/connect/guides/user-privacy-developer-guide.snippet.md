# User privacy guide for app developers

<div class="aui-message note">
    <div class="icon"></div>
    <p>
        <strong>Early Access</strong>
    </p>
    <br/>
    <p>
      Please note that this guide has been released for early access.
      The APIs referenced are still under development and are subject to change.
    </p>
    <br/>
</div>

This guide describes how app developers can comply with user privacy requirements, as detailed
by the [General Data Protection Regulation (GDPR)](https://ec.europa.eu/commission/priorities/justice-and-fundamental-rights/data-protection/2018-reform-eu-data-protection-rules_en).
On this page, you'll find information on your responsibilities as an app developer for Atlassian and
instructions on how to meet these responsibilities.

## GDPR responsibilities for app developers

The GDPR governs the processing of personal data of individuals by an individual, company, or organization.
As an app developer, you must ensure that your apps comply with the GDPR when handling the personal data
for users. This includes:

- [Right to erasure](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-erasure/)
(also known as _Right to be Forgotten_): If your app stores the personal data for a user and the user 
requests for their data to be erased, your app must erase the data.
- [Right to rectification](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-rectification/): If your app stores the personal data for a user and the user changes 
their data, your app must either erase or update the data.
- [Right to be informed](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-be-informed): You must inform users if you collect and use their personal data.

In order to comply with these requirements, **we recommend that your apps do not store any user personal
data** and always retrieve current user data at the time of use using Atlassian APIs. This is the
simplest and most reliable solution, as you don't need to worry about managing and reporting user
personal data for your apps. If you choose this approach, you don't need to read the rest of this guide.

However, if you choose to store user personal data with your apps, Atlassian has built the following
capabilities to help you comply with the GDPR:

- The new _Personal data reporting API_ lets you to report the user accounts that your apps are storing personal data for.
- The reported data is made available to every user on their Atlassian Account profile, so that they
can see which apps are storing their personal data.
- For each user account reported, the _Personal data reporting API_ returns whether each user's personal
data must be erased or refreshed. You must erase or refresh the personal data for your apps accordingly.

Read the following sections on [reporting data](#reportingdata) and [storing data](#storingdata) to
learn how to use these capabilities.

<a name="reportingdata"></a>

## Reporting user personal data for your apps

As an app developer, you are required to periodically report the user personal data that your apps are 
storing. You must report for each accountId (which is a short hand reference to an Atlassian Account ID). 
An accountId uniquely identifies a user across all Atlassian products. It is 1-128 characters long and 
may contain alphanumeric characters as well as `-` and `:` characters. Note that you must use accountIds 
to report personal data usage, even if the API permits other identifiers.

At a high level, this is how to do reporting for your apps:

1. Compile the list of user accounts that your apps are storing personal data for.
1. Use the polling resources for the _Personal data reporting API_ to report the
user accounts for your apps. See the [reference documentation](#reportingapi) below for details.
1. Based on the response, you may need to update or erase the personal data for users accordingly.
1. Repeat this process periodically, as specified by the cycle period.

The cycle period defines the required period of time between sending reports for a given accountId.
You can think about it as the maximum allowable staleness of reported data that is persisted in Atlassian.
By default, the cycle period is **15 days**. However, the polling resources may return a different cycle
period (in the `Cycle-Period` header) that you must follow instead. Note that you
should not send reports more frequently than the cycle period for each account, which helps limit the
load on servers. This is enforced by rate limiting on the polling resources.

When setting up reporting for your apps, also consider the following recommendations:

* **Understand your reporting obligations**: All apps storing personal data must report user personal data, 
using the _Personal data reporting API_.<br> 
Your apps do not need to report when they proactively erase personal data for a user. 
Atlassian will apply a time to live (TTL) on the data reported by apps. This means an app may be 
listed in a user's profile for some period after the app has ceased storing personal data for that user.
* **Cater for interruptions**: The polling iteration period is the period over which an app will iterate 
over all the accountIds to send reports for personal data usage. The longer the polling iteration period, 
the more likely the polling will be interrupted by events such as server restarts and app updates.<br>
This means that apps need to keep track of where they are within a poll period and have 
a means of resuming a poll cycle from this information. If this is too complex to implement, consider 
using a shorter polling iteration period instead.
* **Schedule around potential conflicts**: Consider that scheduled actions by apps generally take place 
at particular times of the day, such as midnight or at the top of each hour. Don't schedule polling 
requests for your apps for specific times, so that there are not conflicts with other apps.
* **Handle account additions and deletions**: The iteration logic for your apps must handle account 
additions and deletions. This will mean adjusting the polling rate and/or batch sizes.

<a name="storingdata"></a>

## Storing user personal data for your apps

In addition to reporting user personal data for your apps, you must ensure that you are storing user
personal data for your apps correctly:

* **Track the age of personal data**: Apps must track the age of the personal data retrieved from Atlassian. 
This must be sent in reports so that Atlassian can determine if the personal data is stale. If an 
app stores multiple aspects of personal data for an account, the age must correspond 
to the oldest time that the personal data was retrieved at.
* **Store a single copy of personal data**: It is imperative that apps are able to report personal 
data usage accurately and reliably. To ensure that all personal data is erased when necessary, we 
recommend that the app only stores a single copy of the personal data within this address 
space and retrieves it when necessary. The obvious choice for an address space is a persistent store 
such as a database table that supports efficient querying by accountId.
* **Erase personal data when uninstalled**: When an app is uninstalled, or consent is revoked in the 
case of 3LO, it should erase personal data that will no longer be needed.

## Testing

The following accountIds should be used by vendors for testing:

- Active: `5be24ad8b1653240376955d2`
- Closed: `5be24ba3f91c106033269289`

There is no fixed accountId that can be used to test for the _updated_ case.

#### Invalid API calls and regression testing

Atlassian will be monitoring correct usage of the API detailed in this guide.
For example, our systems will detect the case of apps repeatedly checking the status
of a closed account beyond a reasonable time frame.
For this reason, repeated/regression testing of the closed account case should only be done
using the closed test account provided above since we have blacklisted this accountId
from our anomaly detection logic.
