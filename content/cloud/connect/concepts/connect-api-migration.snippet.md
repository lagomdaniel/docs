# API migration Opt-in


## Introduction

This Atlassian Connect feature provide a mechanism for apps to signal readiness for new API behaviors as well as selectively enrol during the introduction / deprecation period for the purposes of testing. 

Development of this mechanism was motivated by the introduction of API changes that are not backward compatible (e.g. the removal of legacy user references from our public cloud REST APIs). Such changes will *break* existing apps that do not yet support the new API version. 

## Updating your descriptor

Atlassian Connect apps can subscribe to a new migration via a top-level field called `apiMigrations` located in the [app-descriptor](../app-descriptor/).

### How to opt in

To opt in to active migrations, an app needs to specify a migration key and a migration value. The value for API migrations can be a boolean flag or  in  some cases an enumeration to specify one of  all possible states. 

For example, in order to opt in to the new API behaviors which remove legacy user references from our Connect REST APIs add the following to your app descriptor:

```JSON
{
    "key": "my-app",
    "name": "My App",
    ...,
    "apiMigrations": {
        "gdpr": true
    }
}
```

You'll see that setting the active migration (key = "gdpr") to the value = true changes Atlassian Connect API behaviors so that the app is no longer sent personal data including legacy user references in responses, payloads and web-hook notifications from Atlassian Connect. Note: Jira and Confluence REST APIs require an additional opt in via headers and query parameters which are described in their corresponding API Migration Guides.

This also signals to Atlassian that your app is ready for the deprecation. 

### How to opt out

Apps do not need to update their descriptors to continue using legacy API behaviors during a deprecation period, however, explicitly opting out signals to Atlassian that you are blocked from completing the migration.  When you opt out API behaviors *do not change*. Note: This is only true during active api migrations.  

For example, in order to opt out to the new API behaviors which remove legacy user references from our Connect REST APIs add the following to your app descriptor:

```JSON
{
    "key": "my-app",
    "name": "My App",
    ...,
    "apiMigrations": {
        "gdpr": false
    }
}
```

If you've signaled that you are blocked via your app descriptor you should also raise or watch a corresponding issue so that we know what is causing the blocker and can help resolve it prior to deprecation. 

## Active API migrations

Please note that API migration subscription is only relevant during the period when migration is *active*. If apps specify an API Migration in their descriptor, the provided value will be used to determine their enrolment status, otherwise they are subject to default system behavior. After the migration period has ended, the default value of the API Migration will change and thus enforcing new behavior on all apps, regardless of the value specified in the descriptor. For example, please consider GDPR API migration:

 - **Before the migration period**: the value of API subscription in the app descriptor is ignored.
 - **During the migration period**: app can specify whether they are ready or not to use new APIs by setting `"gdpr": true` in the descriptor.
 - **After the migration period**: all the apps will no longer receive personal data regardless of the value of the `gdpr` field.


You can specify migrations that you would like to opt-in using `apiMigrations` property.

See [active migrations](../connect-active-api-migrations/) for the list of currently active API migrations.
