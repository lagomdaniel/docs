# Conditions

A condition specifies requirements that must be met for a user interface element exposed by an app to be displayed
to a user. Typical use cases include requiring a user to be logged in or to have specific permissions.

Various types of modules accept conditions, including
[Pages](/cloud/jira/platform/modules/page.html), [Web Panels](/cloud/jira/platform/modules/web-panel.html) and [Web Items](/cloud/jira/platform/modules/web-item.html).
To see whether a certain module accepts conditions, see its specific module documentation page.

Conditions may also be used in [context parameters](../context-parameters/#inline-conditions).

There are different classes of conditions. Most commonly used are [the predefined conditions provided by each host product](#static).
When further customization is needed, conditions can be specified [in terms of properties stored by the app in the host product](#entity-property).

These conditions operate on properties of the user at the browser, the entity being viewed (e.g. an issue or a blog post)
or its container (e.g. a project or a space), or the entire system.

Apps can also use conditions as building blocks of [boolean expressions](#boolean-operations) containing the
logical operations conjunction (AND), disjunction (OR), or negation.

## <a name="static"></a>Predefined conditions

A predefined condition is a condition which is exposed from the host Atlassian application.
See [the list of predefined conditions](#product-specific-conditions).

For example, a condition that will evaluate when only logged-in users view the page is specified by the following
module declaration.


``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

### <a name="static-condition-parameters"></a>Condition parameters

Certain predefined conditions accept parameters.

For example, the `has_issue_permission` condition passes only for users who have the permission specified in the
ondition. The issue for which permissions are checked is the issue being viewed by the user at the browser.

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "has_issue_permission",
                        "params": {
                            "permission": "RESOLVE_ISSUE"
                        }
                    }
                ]
            }
        ]
    }
}
```

### <a name="product-specific-conditions"></a>List of predefined conditions

Each product defines a set of conditions relevant to its domain.

#### <a name="common-conditions"></a>Common

* [`entity_property_equal_to`](#property-conditions)
* `feature_flag`
* `user_is_admin`
* `user_is_logged_in`
* `user_is_sysadmin`
* `addon_is_licensed`



#### <a name="jira-conditions"></a>Jira

* [`jira_expression`](#jira-expression-condition)
* `can_attach_file_to_issue`
* `can_manage_attachments`
* `has_issue_permission`
* `has_project_permission`
* `has_selected_project`
* `has_sub_tasks_available`
* `has_voted_for_issue`
* `is_admin_mode`
* `is_issue_assigned_to_current_user`
* `is_issue_editable`
* `is_issue_reported_by_current_user`
* `is_issue_unresolved`
* `is_sub_task`
* `is_watching_issue`
* `linking_enabled`
* `sub_tasks_enabled`
* [`time_tracking_enabled`](#time-tracking-conditions)
* `user_has_issue_history`
* `user_is_project_admin`
* `user_is_the_logged_in_user`
* `voting_enabled`
* `watching_enabled`
* [`can_use_application`](#can-use-application)
* [`jira_time_tracking_provider_enabled`](#time-tracking-conditions)
* [`addon_time_tracking_provider_enabled`](#time-tracking-conditions)
* [`has_attachment`](#has-attachment)
* [`entity_property_contains_any_user_role`](#entity-property-contains-any-user-role)
* [`addon_property_contains_any_user_role`](#entity-property-contains-any-user-role)

##### <a name="jira-servicedesk-conditions"></a>Jira Service Desk conditions
* `servicedesk.is_agent`

    True if user is an agent for a particular Service Desk project
* `servicedesk.is_customer`

    True if user raised or is a request participant for a particular Service Desk issue

##### <a name="jira-condition-parameters"></a>Jira condition parameters


`has_issue_permission`, `has_project_permission` and `has_global_permission` require a key of the permission that
will be checked for the current user. The first two conditions check project permissions and the last one checks global permissions.
Below you will find all the built-in permission keys.

Note that you may also provide any of your custom permission keys
(defined in the [project](../modules/project-permission.html) or [global](../modules/global-permission.html) permission module).
Permissions defined by apps need to be prefixed with an app key followed by two underscores and only then the custom permission key,
for example: `your.add.on.key__yourPermissionKey`.

##### Project permission keys

* `ADD_COMMENTS`
* `ADMINISTER_PROJECTS`
* `ASSIGN_ISSUES`
* `ASSIGNABLE_USER`
* `BROWSE_PROJECTS`
* `CLOSE_ISSUES`
* `CREATE_ATTACHMENTS`
* `CREATE_ISSUES`
* `DELETE_ALL_ATTACHMENTS`
* `DELETE_ALL_COMMENTS`
* `DELETE_ALL_WORKLOGS`
* `DELETE_ISSUES`
* `DELETE_OWN_ATTACHMENTS`
* `DELETE_OWN_COMMENTS`
* `DELETE_OWN_WORKLOGS`
* `EDIT_ALL_COMMENTS`
* `EDIT_ALL_WORKLOGS`
* `EDIT_ISSUES`
* `EDIT_OWN_COMMENTS`
* `EDIT_OWN_WORKLOGS`
* `LINK_ISSUES`
* `MANAGE_WATCHERS`
* `MODIFY_REPORTER`
* `MOVE_ISSUES`
* `RESOLVE_ISSUES`
* `SCHEDULE_ISSUES`
* `SET_ISSUE_SECURITY`
* `TRANSITION_ISSUES`
* `VIEW_DEV_TOOLS`
* `VIEW_READONLY_WORKFLOW`
* `VIEW_VOTERS_AND_WATCHERS`
* `WORK_ON_ISSUES`

##### Global permission keys

* `ADMINISTER`
* `SYSTEM_ADMIN`
* `USER_PICKER`
* `CREATE_SHARED_OBJECTS`
* `MANAGE_GROUP_FILTER_SUBSCRIPTIONS`
* `BULK_CHANGE`


### <a name="property-conditions"></a>Property conditions

Apps that need to impose custom requirements on when user interface elements are displayed can use a property condition.

The following property conditions exist:

 - `entity_property_exists`
 - `entity_property_equal_to`
 - `entity_property_equal_to_context`
 - `entity_property_contains_any`
 - `entity_property_contains_all`
 - `entity_property_contains_context`
 - `entity_property_contains_any_user_group`
 - `entity_property_contains_any_user_role` (Jira only)

These allow fast comparisons to be made against data (properties) stored by the app in the host product. See the
[Storing data without a database](../storing-data-without-a-database/) page for more details. Usually, properties
are set by a REST call against an entity type.

Property conditions are defined in the atlassian-connect.json file in a similar way to built in conditions. An example is below.

``` javascript
{
    "modules": {
        "webItems": [
            {
                "key": "conditional-web-item",
                "name": {
                    "value": "Conditional Web Item"
                },
                "url": "/conditional-web-item",
                "location": "page.metadata.banner",
                "conditions": [
                    {
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "content",
                            "propertyKey": "workflow",
                            "value": "In-Progress",
                            "objectName": "state"
                        }
                    }
                ]
            }
        ]
    }
}
```

The condition requires three parameters and can contain (as in the above example) an optional fourth parameter.

##### Required parameters

* `entity` - the entity on which the property has been stored. If an entity of the expected type is not present in the
  rendering context of the user interface element, the condition evaluates to false.
  * Common: [`addon`](../storing-data-without-a-database/)
  * Confluence: `content`, `space`
  * Jira: `project`, `issue`, `user`, `issuetype`, `comment`, `dashboarditem`
* `propertyKey` - the key of the property to check. If the property is not present, the condition evaluates to false.
* `value` - the value to compare the property value against. This is required for `equal_to` and `contains` conditions.
* `contextParameter` - the context parameter value to read from the current page request to use for comparison. This is
  required for all context conditions except for those that check the group memberships of the browser user.

##### Optional parameters

The optional parameter `objectName` lets you select which part of the entity property JSON value you wish to compare
against. For example, if an entity property had the following value:

``` json
{
    "one": {
        "ignored": "value",
        "two": true
    },
    "also", "ignored"
}
```
Then you could set the `value` parameter to `true` and the `objectName` parameter to `"one.two"` and the condition would
evaluate to `true`. Specifying an `objectName` string which cannot be used to traverse the property value results in `false`.

#### <a name="property-exists-example"></a>Property exists example

You can write a condition that asserts whether on not a property exists against a particular entity. For example, if you
only set the property `specialUrls` on the issues that have your special details then you could write the following
condition to ensure that your web fragment only shows up in the locations that your property does:

``` json
{
    "condition": "entity_property_exists",
    "params": {
        "entity": "issue",
        "propertyKey": "specialUrls"
    }
}
```

If you want to check if a particular field exists within an entity property then you can use the standard `objectName` syntax:

``` json
{
    "condition": "entity_property_exists",
    "params": {
        "entity": "issue",
        "propertyKey": "specialUrls",
        "objectName": "path.to.my.field"
    }
}
```

#### <a name="property-conditions-example"></a>Property equal to example

An app could let administrators activate functionality per Jira project. This could be achieved by storing an entity
property with the key `mySettings` and the value `{"isEnabled" : true}` on each project using the product's REST API.
Then use the `entity_property_equal_to` to test for it with the following module definition:

``` json
{
    "modules": {
        "webPanels": [
            {
                "location": "atl.jira.view.issue.right.context",
                "conditions": [
                    {
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "project",
                            "propertyKey": "mySettings",
                            "objectName": "isEnabled",
                            "value": "true"
                        }
                    }
                ]
            }
        ]
    }
}
```

#### <a name="property-conditions-contains-example"></a>Property contains any example

It is also possible to write conditions where you can check if one or all of the values in the condition are present in the
hosted data.

For example, imagine that you stored the following data against a Jira project under the property key *consumingTeams*:

``` json
{
    "teamCount": 4,
    "teams": ["Legal", "Human Resources", "Accounting", "Taxation"]
}
```

You could then write a condition to only apply to projects that are for the 'Accounting' and 'Development' teams, like so:

``` json
{
    "condition": "entity_property_contains_any",
    "params": {
        "entity": "project",
        "propertyKey": "consumingTeams",
        "objectName": "teams",
        "value": "[\"Accounting\", \"Development\"]"
    }
}
```

You will note that the `value` parameter needs to be a string escaped JSON element.


#### <a name="property-context-paremeter-example"></a>Property context parameter example

The `entity_property_equal_to_context` and `entity_property_contains_context` conditions
let you specify a [context parameter](../concepts/context-parameters.html), from the current HTTP request,
to use for comparison against an entity property. For example, to write a condition that only evaluates to true when a
pre-defined project "Scrum Master" is the one making the HTTP request you would write something like this:

``` json
{
    "condition": "entity_property_equal_to_context",
    "params": {
        "entity": "project",
        "propertyKey": "teamRoles",
        "objectName": "scrumMaster",
        "contextParameter": "user.key"
    }
}
```

As you can see, the primary difference between the usual entity property conditions (other than the name of the condition),
is that there is no `value` field and instead that has been replaced with the `contextParameter` field. In other conditions,
the `value` field must be a JSON literal value; it will be the same value for every HTTP request across all of production
and across every tenant. Instead, for this condition, you must provide a valid Jira or Confluence
[context parameter](../concepts/context-parameters.html) inside the `contextParameter` field and that is the value that
will be used for comparison in the condition at runtime. It is important to note that the "Standard parameters" from the
context parameters cannot be used here. Only the Jira or Confluence specific parameters are allowed.

If a given `contextParameter` cannot be found in the current HTTP request context then the condition will evaluate to
false. For example, a condition that asks for the 'user.key' when there is no logged in user will result in the condition
evaluating to false.

This condition allows you to have per instance configuration that will let you dictate which modules appear to different
users based on contextual information for that request.


#### <a name="property-conditions-aliases"></a>Aliases

The following aliases can be used which make writing property conditions more convenient.

__Confluence__

* `content_property_equal_to`
* `content_property_equal_to_context`
* `content_property_contains_any`
* `content_property_contains_all`
* `content_property_contains_context`
* `content_property_contains_any_user_group`
* `space_property_equal_to`
* `space_property_equal_to_context`
* `space_property_contains_any`
* `space_property_contains_all`
* `space_property_contains_context`
* `space_property_contains_any_user_group`

__Common__

* `addon_property_equal_to`
* `addon_property_equal_to_context`
* `addon_property_contains_any`
* `addon_property_contains_all`
* `addon_property_contains_context`
* `addon_property_contains_any_user_group`

When using these aliases, the `entity` param is not used. This would mean the following two condition definitions are identical.

``` json
{
	"condition": "entity_property_equal_to",
	"params": {
		"entity": "content"
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

``` json
{
	"condition": "content_property_equal_to",
	"params": {
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

### <a name="addon-is-licensed-condition"></a>addon\_is\_licensed condition

The `addon_is_licensed` condition will evaluate to `true` if and only if your app is a paid app and it
is licensed. This condition can be placed on any Atlassian Connect module that supports conditions; it is not context sensitive.
Here is an example of the new condition in use:

``` json
"jiraIssueTabPanels": [{
    "conditions": [
        {
            "condition": "addon_is_licensed"
        },
        {
            "condition": "user_is_logged_in"
        }
    ],
    "key": "your-module-key",
    "name": {
        "value": "Your module name"
    },
    "url": "/panel/issue?issue_id={issue.id}&issue_key={issue.key}",
    "weight": 100
}]
```
In this example the Jira Issue Tab Panel will only be shown if the app is licensed and the user is logged in.

If you give away your app for _free_ then you must not use the `addon_is_licensed` condition. This is important because
all free apps are considered _unlicensed_ and will thus the condition will return `false`. Only use this condition with
paid apps that are licensed via the Atlassian Marketplace.

### <a name="can-use-application"></a>can\_use\_application condition

`can_use_application` condition checks whether the current user is allowed to use a specific application
 (like Jira Software or Jira Service Desk).

 The condition is true if and only if both of the the following statements are true:

 * the application is installed and enabled on the Jira instance
 * the user is permitted to use the application according to the installed license

The condition requires an `applicationKey` parameter, for example:

 ``` json
 {
     "modules": {
         "generalPages": [
             {
                 "conditions": [
                     {
                         "condition": "can_use_application",
                         "params": {
                            "applicationKey": "jira-software"
                         }
                     }
                 ]
             }
         ]
     }
 }
 ```

Supported application keys are:

* `jira-core`
* `jira-servicedesk`
* `jira-software`

If an unrecognized application key is provided then the condition will simply evaluate to `false`.

### <a name="project-type"></a>project\_type condition

`project_type` condition checks whether the current project matches a specific project key.

 The condition is true if and only if both of the the following statements are true:

 * the user is viewing a page that has a contextual project (e.g. the view issue page)
 * the project key of the contextual project matches the project key specified

The condition requires a `projectTypeKey` parameter, for example:

 ``` json
 {
     "modules": {
         "generalPages": [
             {
                 "conditions": [
                     {
                         "condition": "project_type",
                         "params": {
                            "projectTypeKey": "business"
                         }
                     }
                 ]
             }
         ]
     }
 }
 ```

Supported project type keys are:

* business
* service_desk
* software
* ops

### <a name="jira-expression-condition"></a>jira\_expression condition

The `jira_expression` condition is a flexible way of implementing custom condition logic using [Jira expressions](../jira-expressions).

A Jira expression condition evaluates to true only if the provided Jira expression evaluates to true. 
It will evaluate to false in all other cases, including:

* The Jira expression fails to evaluate because of errors.
* The Jira expression returns anything other than a boolean value.

#### Example

The following example declares a condition that shows the web element only if:

* The app has the property `supportedIssueTypes`, which is a list of issue type IDs.
* The current issue has an issue type with ID stored on the list.

 ``` json
{
  "conditions": [
   {
     "condition": "jira_expression",
     "params": {
       "expression": "app.properties['supportedIssueTypes'].includes(issue.issueType.id)"
     }
   }
 ]

 ```

The example uses the `app` [context variable](../jira-expressions#context-variables) to query the app properties
and the `issue` context variable to examine the type of the current issue.

#### Context variables

The following context variables are available to the web condition expressions:

* `user` ([User](../jira-expressions-type-reference#user)): The logged in user. 
* `app` ([App](../jira-expressions-type-reference#app)): The app that provided the condition.
* `issue` ([Issue](../jira-expressions-type-reference#issue)): The currently displayed issue.
* `project` ([Project](../jira-expressions-type-reference#project)): The currently selected project. 
* `sprint` ([Sprint](../jira-expressions-type-reference#sprint)): The currently displayed sprint. 
* `board` ([Board](../jira-expressions-type-reference#board)): The currently displayed board. 

Note that some of these variables can be `null`. For example, if the request is anonymous then `user` will be `null`;
if the web element is not displayed in the context of an issue or project, then `issue` and `project` will be `null`.
You can use boolean operators to check if a variable is defined, for example:<br>
`issue != null && issue.comments.length > 0`.

### <a name="time-tracking-conditions"></a>Time tracking conditions

There are three time tracking conditions available:
- `time_tracking_enabled` is true if time tracking is enabled, regardless of which time tracking provider is selected
- `jira_tracking_provider_enabled` is true if the currently selected time tracking provider is Jira's native time tracking implementation
- `addon_time_tracking_provider_enabled` is true if the currently selected time tracking provider matches the one defined in the condition parameters

This `addon_time_tracking_provider_enabled` condition can be used with the [Time Tracking Provider](../modules/time-tracking-provider/) module to check if the app's time tracking implementation is currently selected in Jira.

For example, if your app has the key `app-key` and has declared a `jiraTimeTrackingProviders` module with the key `module-key`, such as:

``` json
{
  "key": "app-key",
  "modules": {
    "jiraTimeTrackingProviders" : [
      {
        "key": "module-key",
        "name": "My time tracking module"
      }
    ]
  }
}
```

Then you can add a condition to the other modules in your app that comprise your time tracking implementation, like this:

``` json
{
  "condition": "addon_time_tracking_provider_enabled",
  "params": {
    "addonKey": "app-key",
    "moduleKey": "module-key"
  }
}
```

If an unrecognized `addonKey` or `moduleKey` is provided then the condition will simply evaluate to `false`.

### <a name="has-attachment"></a>has\_attachment condition

The `has_attachment` condition can be used to check whether the currently displayed issue has any attachments.
Additionally, an optional `extension` param may be provided
to check if it has an attachment with a specified file extension (case insensitive).

For example, to check if the issue has any PDF files attached, the condition should be specified thus:

``` json
{
  "condition": "has_attachment",
  "params": {
    "extension": "pdf"
  }
}
 ```

### <a name="entity-property-contains-any-user-role"></a>entity_property_contains_any_user_role condition

The `entity_property_contains_any_user_role` condition checks whether or not the browser user is in any Jira project
role that is also in a list of roles stored in an entity property.
It is aliased by the `addon_property_contains_any_user_role` condition, for which the `entity` parameter is omitted.

This is very similar to the `entity_property_contains_any_user_group` condition.

For example, if you wish to show a web panel on the Jira view issue page only to users who have at least one of that
project's roles then you could use the following condition.

 ``` json
 {
   "condition": "entity_property_contains_any_user_role",
   "params": {
     "entity": "project",
     "propertyKey": "keyOfPropertyContainingMyListOfRoles",
     "objectName": "someObject"
   }
 }
 ```

This references an entity property with the key `keyOfPropertyContainingMyListOfRoles` stored on the issue's project. To evaluate to `true` it should contain data like the following and the page should be viewed by a user who has the `developers` role.

 ``` json
 {
   "someObject": [ "developers" ]
 }
 ```

The `objectName` parameter is optional. If omitted then the equivalent entity property value would simply be the array
moved up to the root level.

 ``` json
 [ "developers" ]
 ```

It is equally possible to store the integer role id instead of its name. This will be less readable but does not change when
administrators change role names, so it can be an advantage in some situations.
Here is the above example changed to use a role id:

 ``` json
 [ 1234 ]
 ```

Mixing role names and ids is also ok:

 ``` json
 [ "developers", 1234 ]
 ```

## <a name="boolean-operations"></a>Boolean operations

The [Composite Condition](../modules/fragment/composite-condition.html) module fragment can be used wherever a condition
is expected. This allows the construction of boolean expressions aggregating multiple conditions.

For example, the [Entity Property tool](https://ep-tool.useast.atlassian.io/jira/atlassian-connect.json) has a condition that evaluates to true when the user is logged in and:

- the entity property tool is NOT disabled for everybody OR
- the entity property tool is enabled for me.

Written as a composite condition that looks like this:


``` json
{
    "modules": {
        "webItems": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    },
                    {
                        "or": [
                            {
                                "condition": "entity_property_equal_to",
                                "invert": true,
                                "params": {
                                    "entity": "addon",
                                    "propertyKey": "ep-tool.disabled-for-all",
                                    "value": "true"
                                }
                            },
                            {
                                "condition": "entity_property_equal_to",
                                "params": {
                                    "entity": "user",
                                    "propertyKey": "ep-tool.enabled-for-me",
                                    "value": "true"
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
```

Composite conditions allow "or" and "and" conditions to be written for Atlassian Connect apps. You can also get a NOT operation by using the "invert" field on single conditions; as the above example shows.

Note: The top most `conditions` block is implicitly a composite "and" condition.
