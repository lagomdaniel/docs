# Jira expressions

## Introduction

Jira expressions is a domain-specific language designed with Jira in mind, evaluated on the Jira Cloud side.
It can be used to evaluate custom code in the context of Jira entities.  

There are currently four ways to use Jira expressions:

* [REST API](/cloud/jira/platform/rest/v2/#api-rest-api-2-expression-eval-post)
* [Web condition](../conditions/#jira-expression-condition)
* [Workflow condition](../modules/workflow-condition)
* [Workflow validator](../modules/workflow-validator)

The REST endpoint can be used to test the expressions that you plan to use elsewhere, or to load data in a flexible way. 
For example, if you would like to build an efficient and lightweight visualization of issue comments, 
you could fetch a minimal required set of data (id, author and the content excerpt) with the following expression:

```javascript
issue.comments.map(c => { 
    id: c.id, 
    author: c.author.displayName, 
    excerpt: c.body.plainText.slice(0, 200) + '...'
})
```

The web condition gives you almost infinite control over the visibility of your web elements. 
For example, you could use the following expression to show your panel only if the current user commented on the current issue:

```javascript
issue.comments
     .filter(c => c.author.accountId == user.accountId)
     .length > 0
```


## Syntax and semantics

Jira expressions follow JavaScript syntax. You can think of them as a JavaScript dialect.

The following constructs are supported (this list may not be exhaustive):

* **Static and computed member access** 
   Static member access is used when you want to access an object's field and know the field's name at the time of writing the expression. 
   For example, `issue.key` is an expression that accesses the `key` field from `issue`. 
   Computed member access is used when you want to dynamically create the name of the field you are accessing, 
   or if the name contains special characters, in which case accessing the field using the static member access will not be allowed by the syntax. 
   It is especially useful when accessing entity properties, which usually contain dots or dashes in their names. 
   For example, `issue.properties['com.your.app.property-name']`.

* **Indexed access** 
   Individual members of lists can be accessed by index. 
   For example, to get the first issue comment, write: `issue.comments[0]`.

* **Mathematical operators** 
   Jira expressions allow all the usual kinds of mathematical operations. 
   You can add, subtract, multiply, or divide numbers. 
   For example, to check if the number of comments on an issue is even, write: `issue.comments.length % 2 == 0`.

* **Boolean operators** 
   The usual logical operators are available: conjunction (`&&`), disjunction (`||`) and negation (`!`). 
   If used with boolean values (`true` or `false`), their behavior follows the rules of classical boolean algebra. 
   Each of these operators can also be used with any type, following the [JavaScript semantics](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators) in this case. 
   The latter is especially useful for defining default values. 
   For example, to get the value of the issue property `"myProperty"` while also providing a default value in case it's not defined, write: 
   `issue.properties["myProperty"] || "default value"`.

* **Comparisons** 
   Values can be compared to each other in different ways, depending on the type. For example, 
   it's possible to check if one _number_ is lesser or greater than another number, but _lists_ or _strings_ can be tested only for equality.
    Only values of the same type can be compared together. 
   For example, to check if the issue has more than 0 comments, write: `issue.comments.length > 0`.
* **Conditional expressions** 
   Conditional expressions can be used when different results should be returned depending on a condition. 
   For example, to get the first comment's author, we would first need to check if there are comments at all: 
   
```javascript
issue.comments.length > 0 ? issue.comments[0].author : null
```
  
* **Arrow functions** 
   Jira expressions by design do not support classical imperative loops. 
   Instead, they follow the modern paradigm of functional programming and provide a set of [built-in List processing methods](../jira-expressions-type-reference#list), 
   along with the syntax for arrow functions, also knows as lambdas. 
   These functions are written in the form of _x => y_, 
   where _x_ is the name of the variable that can be used in the function's body, denoted here as _y_. 
   For example, to return the number of comments with contents longer than 100 characters, 
   first map the comments to their texts, then filter them to leave only those long enough:
    
```javascript
issue.comments
     .map(c => c.body.plainText)
     .filter(text => text.length > 100)
     .length
```
   
* **List literals** 
   [Lists](../jira-expressions-type-reference#list) can not only be obtained from context objects but also created manually. 
   For example, to check if the issue type is either a Bug or Task, 
   create a list with these two types and use the `includes()` method to test if the actual value is one of the two listed:
    
```javascript
['Bug', 'Task'].includes(issue.issueType.name)
```
    
* **Object literals** 
   Jira expressions can return structured pieces of data with the use of object literals. 
   For example, to return only comments' authors and contents instead of the entire comments, 
   create an object containing these two fields for each comment: 
   
```javascript
issue.comments.map(c => { 
    author: c.author, 
    body: c.body.plainText 
})
```
   
## Examples

The following examples demonstrate how to write Jira expressions and what they can do.

Get contents of all comments added by the current user in the current issue: 

```javascript
issue.comments
     .filter(c => c.author.accountId == user.accountId)
     .map(c => c.body)
```

Check if the current user is the one stored in a project's entity property: 

```javascript
user.accountId == project.properties['special-user'].accountId
```

Check if the issue type is either a Bug or Task (using a regular expression): 

```javascript
issue.issueType.name.match('^(Bug|Task)$') != null
```

Retrieve IDs of all linked issues along with the link name: 

```javascript
issue.links.map(link => { 
    name: link.type[link.direction], 
    issue: link.linkedIssue.id 
}) 
```

## Data aggregation

To aggregate data, use the `set()` method for [Map](../jira-expressions-type-reference#map) 
and the `reduce()` method for [List](../jira-expressions-type-reference#list).

This is particularly useful in combination with a JQL query that can be provided in the [REST API](/cloud/jira/platform/rest/v2/#api-rest-api-2-expression-eval-post) 
to load a list of issues into the [expression context](#context-variables). 

For example, the following expression will count issues by their status name:

```javascript
issues.reduce((result, issue) => 
            result.set(
                issue.status.name, 
                (result[issue.status.name] || 0) + 1), 
            new Map())
```

Note that `map[issue.status.name] || 0` will return either the current mapping for the given status, 
or `0` if there is no mapping yet. This is a handy way to declare default values.

If the above expression is evaluated using the [REST API](/cloud/jira/platform/rest/v2/#api-rest-api-2-expression-eval-post), 
the result will be, for example:

```json
{
    "value": {
        "To Do": 2,
        "In Progress": 10,
        "Done": 5
    }
}
```

## Context variables

Depending on the context in which a Jira expression is evaluated, different context variables may be available:

* `user` ([User](../jira-expressions-type-reference#user)): The current user. Equal to `null` if the request is anonymous. 
* `app` ([App](../jira-expressions-type-reference#app)): The [Connect app](https://developer.atlassian.com/cloud/jira/platform/integrating-with-jira-cloud/#atlassian-connect) 
         that made the request or provided the module. 
         Always available for expressions used in Connect modules, and also in REST API request made by Connect Apps 
         (read more here: [Authentication for Connect apps](https://developer.atlassian.com/cloud/jira/platform/authentication-for-apps/)). 
* `issue` ([Issue](../jira-expressions-type-reference#issue)): The current issue. 
* `issues` ([List](../jira-expressions-type-reference#list)<[Issue](../jira-expressions-type-reference#issue)>): The list of issues 
            available when a JQL query is specified in the request context when using the [REST API](/cloud/jira/platform/rest/v2/#api-rest-api-2-expression-eval-post). 
* `project` ([Project](../jira-expressions-type-reference#project)): The current project. 
* `sprint` ([Sprint](../jira-expressions-type-reference#sprint)): The current sprint. 
* `board` ([Board](../jira-expressions-type-reference#board)): The current board. 

These variables are registered in the global scope. For example, to check if the request is not anonymous, write: `user != null`.

## Entity properties

Using Jira expressions, it is possible to access entity properties of any entity that supports them, that is: 
issue, project, issue type, comment, user, board, sprint. 
[App properties](https://developer.atlassian.com/cloud/jira/platform/storing-data-without-a-database/#a-id-app-properties-a-app-properties) 
are also available. 
To do this, get the `properties` field of the appropriate object. 
For example, _app.properties_. The field returns what can be thought of as a map of all properties, indexed by their keys.

Read more about how to interact with properties in the 
[EntityProperties](../jira-expressions-type-reference#entity-properties) object documentation.

## Date and time

Fields that contain timestamps, such as `issue.created` or `issue.resolutionDate`, 
are returned as objects of the [Date](../jira-expressions-type-reference#date) type, 
which is based on the [JavaScript Date API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date).

Fields that contain dates only, such as `issue.dueDate`, are returned as the timezone-agnostic [CalendarDate](../jira-expressions-type-reference#calendardate) type, which is like [Date](../jira-expressions-type-reference#date), 
but with a limited set of methods (methods related to time or timezones are not available).

A [Date](../jira-expressions-type-reference#date) or [CalendarDate](../jira-expressions-type-reference#calendardate) object can be transformed into three different String formats:

* **ISO format**. For example, _2018-06-29T12:16:37.471Z_ ([Date](../jira-expressions-type-reference#date) format) or _2018-06-29_ ([CalendarDate](../jira-expressions-type-reference#calendardate) format). 
   To transform a date to this format, call the `toISOString()` method. This will return a string in the ISO 8601 extended format. For example, `issue.created.toISOString()`.
* **Jira REST API format**. For example, _2018-06-29T22:16:37.471+1000_ ([Date](../jira-expressions-type-reference#date) format) or _2018-06-29_ ([CalendarDate](../jira-expressions-type-reference#calendardate) format). 
   Returning dates from Jira expressions renders them in the Jira REST API format. For example, `issue.created`.
* **Human-readable format**. For example, _29/Jun/18 10:16 PM_ ([Date](../jira-expressions-type-reference#date) format) or _29/Jun/18_ ([CalendarDate](../jira-expressions-type-reference#calendardate) format). 
   To transform a date to this format, call the `toString()` method. This will return a string in the human-readable format, according to the current user's locale and timezone. For example, `issue.created.toString()`. The same format is also used if a date is concatenated with a string. For example, `'Due date: ' + issue.dueDate`.

A `Date` object can also be converted to a `CalendarDate` object by using either `toCalendarDate()` or `toCalendarDateUTC()`. 
These methods remove the time information from the object, leaving only the calendar date, 
in the current user's timezone or the UTC timezone, respectively.

[Date](../jira-expressions-type-reference#date) objects of the same type can be compared using regular comparison operators. 
For example, to get comments that were added after the issue's due date, 
write: 

```javascript
issue.comments
     .filter(c => c.created.toCalendarDate() > issue.dueDate)
```

A date can be modified by adding or subtracting units of time. To do this, use the methods below. 
Each of these methods take a date and a number of units of time, then create a new modified date.

* `date.plusMonths(Number)`: Creates a new date that is the original date plus the specified number of months.
* `date.plusDays(Number)`: Creates a new date that is the original date plus the specified number of days.
* `date.plusHours(Number)`: Creates a new date that is the original date plus the specified number of hours.
* `date.plusMinutes(Number)`: Creates a new date that is the original date plus the specified number of minutes.

(All methods above have a subtraction counterpart. For example, `date.minusMonths(Number)`.)

Date modification methods can be used to build expressions that assert when Jira events have occurred. 
To do this, get the current date and modify it, then compare the modified date to the date of the event. 
Here's an example of how to check if an issue has been updated in the last three days:

1. Get the current date. To do this, create a new Date object (that is, `new Date()`).
2. Modify the date, as desired. For example, `new Date().minusDays(3)` is the current date minus three days.
3. Compare the modified date to the date of the `issue.updated` event. For example, `issue.updated > new Date().minusDays(3)` 
will return true if the issue has been updated in the last three days.

## Restrictions

Some restrictions apply to the evaluation of expressions. 
While the limits are high enough not to interfere with any intended usage, it's important to realize that they do exist:

* Some lists are limited to 50 elements in the final result. This is similar to [pagination](/cloud/jira/platform/rest/#expansion) and serves the same purpose. 
  For example, a list of issue comments will always be trimmed to at most 50 items. 
  (Use the `slice()` method to control which portion of the list to return.)
* The expression can execute at most 10 expensive operations (expensive operations are those that load additional data, 
  such as entity properties, comments, or custom fields).
* The max number of results returned in the response is 10,000 primitive values or 1,000 Jira REST API beans.
* The expression's length is limited to 1,000 characters or 100 syntactic elements.

You can use execute your expression with the [REST API](/cloud/jira/platform/rest/#api-rest-api-2-expression-eval-post) 
and use the _meta.complexity_ [expand](/cloud/jira/platform/rest/#expansion) parameter 
to see the complexity of the expression and how close it is to reaching the limits.


## Additional Resources

* [Jira Expressions Type Reference](../jira-expressions-type-reference/)