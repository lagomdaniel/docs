# Security for Connect apps

Atlassian Connect is an app framework used to build Atlassian apps. Atlassian Connect apps are used to extend Atlassian Cloud products, like Jira Cloud, and can extend the user interface, access the APIs, etc. If you are not using Connect to build your integration, see [Security for other integrations](../security-for-other-integrations) instead.

Authentication and authorization are the two key parts of implementing security. Authentication tells Jira Cloud the identity of your app, and authorization determines what actions it can take within Jira. Connect apps use JWT for authentication and either scopes or user impersonation for authorization.

## Authentication 

Atlassian Connect apps use JWT (JSON Web Tokens) for authentication. This is already built into the supported Atlassian Connect libraries.

When an app is installed, a security context is exchanged with the application. This context is used to create and validate JWT tokens that are embedded in API calls. The use of JWT tokens guarantees that:

*   Jira Cloud can verify it is talking to the app, and vice versa (authenticity).
*   None of the query parameters of the HTTP request, nor the path (excluding the context path), nor the HTTP method, were altered in transit (integrity).

To learn more, read [Authentication for Connect apps](../authentication-for-apps/).

## Authorization

Atlassian Connect apps can use two types of authorization: 

*   **Authorization via scopes and app users**: This is the default authorization method. You should use this for most cases.
*   **Authorization with user impersonation**: You should only use this method when your app needs to make server-to-server requests on behalf of a user.

### Authorization via scopes and app users 

This method has two levels of authorization: 

*	Static authorization via scopes 
*	Run-time authorization via app users

Scopes are defined in the app descriptor and statically specify the maximum set of actions that an app may perform: read, write, etc. This security level is enforced by Atlassian Connect and cannot be bypassed by app implementations. To learn more, read our page on [scopes](../scopes/).

Every app is assigned its own app user in a Cloud instance. In general, server-to-server requests are made by the app user. In some situations, the configuration of permission or issue security schemes in Jira Cloud by an administrator can cause an app user not to have permission to make a request. In Jira Cloud, Atlassian Connect will automatically attempt to resolve these errors each time an app is updated. Client-side requests are made as the current user in the browser session, and are supported via the [`AP.request()` method](../jsapi/request/), or the app needs to make a server-to-server request using OAuth 2.0 user impersonation (see next section).

<div class="aui-message tip">
    <div class="icon"></div>
    <p class="title">
        <strong>Combining static and run-time authorization</strong>
    </p>
    <p>
    The set of actions that an app is capable of performing is the intersection of the static scopes and the permissions of the user assigned to the request. This means that requests can be rejected because the assigned user lacks the required permissions. Therefore, your app should always defensively detect HTTP 403 forbidden responses from the product.
    </p>
</div>

### Authorization with user impersonation

User impersonation allows your integration to access Jira APIs on a user's behalf. This is provided via the [JWT Bearer token](https://tools.ietf.org/html/rfc7523#section-2.1) authorization grant type for [OAuth 2.0](https://tools.ietf.org/html/rfc6749), which is also known as two-legged OAuth with impersonation (2LOi). At a high level, this method works by the app exchanging a JWT for an OAuth 2.0 access token (provided by the application). The access token can be used to make server-to-server calls, on behalf of the user, to the application's API.

To learn more, read our page on [User impersonation for Connect apps](../oauth-2-jwt-bearer-token-authorization-grant-type).