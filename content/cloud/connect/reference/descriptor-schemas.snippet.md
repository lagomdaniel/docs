## Descriptor JSON schemas

The descriptor format for apps to each product is defined by a [JSON schema](http://json-schema.org).

* [Confluence](https://bitbucket.org/atlassian/connect-schemas/raw/master/confluence-global-schema.json)
* [Jira](https://bitbucket.org/atlassian/connect-schemas/raw/master/jira-global-schema.json)

### Validator tool

Atlassian Connect provides a stand-alone validation service for app descriptors.

* https://atlassian-connect-validator.herokuapp.com/validate

The validator will check that your descriptor is syntactically correct. Just paste the JSON content
of your descriptor in the `descriptor` field, and select the Atlassian product you want to validate
against.
