---
title: "Object hydration"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning 
date: "2018-06-12"
---

# Response object hydration

Object hydration allows your app to respond to proxied API requests with a minimal amount of data for a resource and Bitbucket will add the full object from the Bitbucket database. This API proxy service will automatically fill in any missing elements of certain Bitbucket data types returned by a remote service. This allows remote services to keep track of only the absolute minimum information to uniquely identify an object.

For example, if your app were meant to get details of a single repository it only has to store a way to identify the repository, which can be as little as a repository UUID. Without using the proxy module, a simple request using a repository UUID returns a partial repository response object, without being hydrated, that would look like this:

``` javascript
{
  "type": "repository",
  "uuid": "{aa5acc33-e5f7-43e9-883d-50325fc68ca8}"
}
```

Using the proxy module, when the response is passed to Bitbucket's API proxy, the proxy:

1. Decodes the JSON from the remote service.
1. Scans the object graph for type elements defined in Bitbucket.
1. For each type, queries the database and substitutes it for the full object.
1. Serializes the resulting object, producing layouts consistent with other, core APIs.

The hydrated response coming from the API proxy provided to the client would look like the entire response body from the [repository](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D) endpoint.

``` javascript
{
    "scm": "git",
    "website": "",
    "has_wiki": false,
    "uuid": "{aa5acc33-e5f7-43e9-883d-50325fc68ca8}",
    "links": {
        "watchers": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/watchers"
        },
        "branches": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/refs/branches"
        },
        "tags": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/refs/tags"
        },
        "commits": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/commits"
        },
        "clone": [
            {
                "href": "https://bitbucket.org/atlassian_tech_writing/teams-in-space-tutorial-content.git",
                "name": "https"
            },
            {
                "href": "git@bitbucket.org:atlassian_tech_writing/teams-in-space-tutorial-content.git",
                "name": "ssh"
            }
        ],
        "self": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content"
        },
        "source": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/src"
        },
        "html": {
            "href": "https://bitbucket.org/atlassian_tech_writing/teams-in-space-tutorial-content"
        },
        "avatar": {
            "href": "https://bitbucket.org/atlassian_tech_writing/teams-in-space-tutorial-content/avatar/32/"
        },
        "hooks": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/hooks"
        },
        "forks": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/forks"
        },
        "downloads": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/downloads"
        },
        "pullrequests": {
            "href": "https://api.bitbucket.org/2.0/repositories/atlassian_tech_writing/teams-in-space-tutorial-content/pullrequests"
        }
    },
    "fork_policy": "allow_forks",
    "name": "Teams In Space Tutorial Content",
    "project": {
        "key": "PROJ",
        "type": "project",
        "uuid": "{a18967d5-acba-4f73-bf9c-36d9fa6ea143}",
        "links": {
            "self": {
                "href": "https://api.bitbucket.org/2.0/teams/atlassian_tech_writing/projects/PROJ"
            },
            "html": {
                "href": "https://bitbucket.org/account/user/atlassian_tech_writing/projects/PROJ"
            },
            "avatar": {
                "href": "https://bitbucket.org/account/user/atlassian_tech_writing/projects/PROJ/avatar/32"
            }
        },
        "name": "Atlassian Tech Writing Scripts"
    },
    "language": "",
    "created_on": "2015-08-14T01:18:36.773253+00:00",
    "mainbranch": null,
    "full_name": "atlassian_tech_writing/teams-in-space-tutorial-content",
    "has_issues": false,
    "owner": {
        "username": "atlassian_tech_writing",
        "display_name": "Atlassian Technical Writing",
        "type": "team",
        "uuid": "{bb95f287-ac12-4a77-986f-7a5208f81c5e}",
        "links": {
            "self": {
                "href": "https://api.bitbucket.org/2.0/teams/atlassian_tech_writing"
            },
            "html": {
                "href": "https://bitbucket.org/atlassian_tech_writing/"
            },
            "avatar": {
                "href": "https://bitbucket.org/account/atlassian_tech_writing/avatar/32/"
            }
        }
    },
    "updated_on": "2015-08-14T07:19:10.128919+00:00",
    "size": 33348,
    "type": "repository",
    "slug": "teams-in-space-tutorial-content",
    "is_private": false,
    "description": "This repository is for storing demo content that can be access for tutorials within product documentation."
}
```

## <a id="capture-parent-objects"></a>Capturing the parent object of the object you're capturing

For objects that belong to another object -- like how a repository belongs to a user or team -- **the parent object may need to be resolved in order to resolve a child object**.

One exception to this is resolving repository objects. If you resolve a repository object using a repository UUID you don't need to resolve the account it belongs to. The UUID of a repository alone is enough to resolve a repository object. You might prefer to resolve repositories using a UUID because it's less data for an application to track, or because a repository can still be resolved if it is transferred between accounts.

Alternatively, if a repository slug is being used to resolve a repository object you will still need to resolve the account a repository object belongs to (the parent object) so Bitbucket can identify the repository.

## <a id="hydration-objects"></a>Response objects that can be hydrated

The response objects listed below are the Bitbucket objects that can be hydrated.

For objects with multiple schemas, you could use any of the listed schemas.

## <a id="issue-schema"></a>Issue schema

``` javascript
{
  "type": "issue",
  "id": issue id, // int
  "repository": *repository schema
}
```

* See [repository schemas](/cloud/bitbucket/proxy-object-hydration/#repository-schemas).

## <a id="snippet-schema"></a>Snippet schema

``` javascript
{
  "type": "snippet",
  "id": snippet id // unicode
}
```

## <a id="pullrequest-schema"></a>Pull request schema

``` javascript
{
  "type": "pullrequest",
  "id": pullrequest id, // int
  "destination": {
    "repository": *repository schema
  }
}
```

\* See [repository schemas](/cloud/bitbucket/proxy-object-hydration/#repository-schemas).

## <a id="repository-schemas"></a>Repository schemas

``` javascript
{
  "type": "repository",
  "uuid": repository uuid // unicode
}
```

``` javascript
{
  "type": "repository",
  "full_name": repository fullname // unicode
}
```

``` javascript
{
  "type": "repository",
  "slug": repository slug, // unicode
  "owner": *account schema
}
```

\* See [account schemas](/cloud/bitbucket/proxy-object-hydration/#account-schemas).

## <a id="account-schemas"></a>Account schemas

### Account (user) schemas

``` javascript
{
  "type": "user",
  "uuid": user uuid // unicode
}
```

``` javascript
{
  "type": "user",
  "account_id": user account id // unicode
}
```

### Account (team) schemas

``` javascript
{
  "type": "team",
  "uuid": team uuid // unicode
}
```

``` javascript
{
  "type": "team",
  "username": team username // unicode
}
```

``` javascript
{
  "type": "team",
  "account_id": team account id // unicode
}
```

## Project schemas

``` javascript
{
  "type": "project",
  "uuid": project uuid // unicode
}
```

``` javascript
{
  "type": "project",
  "key": project key, // unicode
  "owner": *account schema
}
```

\* See [account schemas](/cloud/bitbucket/proxy-object-hydration/#account-schemas).

## Issue comment schema

``` javascript
{
  "type": "issue_comment",
  "id": issue comment id,
  "issue": *issue schema
}
```

\* See [issue schema](/cloud/bitbucket/proxy-object-hydration/#issue-schema).

## Commit comment schema

``` javascript
{
  "type": "commit_comment",
  "id": commit comment id, // int
  "commit": *commit schema
}
```

\* See [commit schema](/cloud/bitbucket/proxy-object-hydration/#commit-schema).

## Pull request comment schema

``` javascript
{
  "type": "pullrequest_comment",
  "id": pullrequest comment id, // int
  "pullrequest": *pullrequest schema
}
```

\* See [pull request schema](/cloud/bitbucket/proxy-object-hydration/#pullrequest-schema).

## Build schemas

``` javascript
{
  "type": "build",
  "uuid": build uuid // unicode
}
```

``` javascript
{
  "type": "build",
  "key": build key, // unicode
  "repository": *repository schema,
  "commit": **commit schema,
  "refname": refname // unicode
}
```

\* See [repository schemas](/cloud/bitbucket/proxy-object-hydration/#repository-schemas).

** See [commit schema](/cloud/bitbucket/proxy-object-hydration/#commit-schema).

## <a id="commit-schema"></a>Commit schema

``` javascript
{
  "type": "commit",
  "repository": *repository schema,
  "hash": commit hash // unicode
}
```

\* See [repository schemas](/cloud/bitbucket/proxy-object-hydration/#repository-schemas).

## Snippet commit schema

``` javascript
{
  "type": "snippet_commit",
  "snippet": *snippet schema,
  "hash": commit hash // unicode
}
```

\* See [snippet schema](/cloud/bitbucket/proxy-object-hydration/#snippet-schema).

## Commit file schema

``` javascript
{
  "type": "commit_file",
  "commit": *commit schema,
  "path": path to file // unicode
}
```

\* See [commit schema](/cloud/bitbucket/proxy-object-hydration/#commit-schema).

## Commit directory schema

``` javascript
{
  "type": "commit_directory",
  "commit": *commit schema,
  "path": path to directory // unicode
}
```

\* See [commit schema](/cloud/bitbucket/proxy-object-hydration/#commit-schema).

---------------------

## Rendered schemas

``` javascript
{
  "type": "rendered",
  "raw": raw content to render,
  ???
}
```

## Error schema

``` javascript
{
  "type": "error",
  "error": {
    "message": error message,
    "id": error id,
    "data": error data,
    "fields": error fields,
    "detail": error detail
  }
}
```
