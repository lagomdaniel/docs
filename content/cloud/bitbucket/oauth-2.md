---
title: "OAuth 2.0"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: security 
date: "2018-11-06"
---

# OAuth 2.0

Our OAuth 2 implementation is merged in with our existing OAuth 1 in
such a way that existing OAuth 1 consumers automatically become
valid OAuth 2 clients. The only thing you need to do is edit your
existing consumer and configure a callback URL.

Once that is in place, you'll have the following 2 URLs:

- `https://bitbucket.org/site/oauth2/authorize`
- `https://bitbucket.org/site/oauth2/access_token`


For obtaining access/bearer tokens, we support all four of RFC-6749's grant
flows, plus a custom Bitbucket flow for exchanging JWT tokens for access tokens:


### 1. Authorization Code Grant (4.1)

The full-blown 3-LO flow. Request authorization from the end user by
sending their browser to: `https://bitbucket.org/site/oauth2/authorize?client_id={client_id}&response_type=code`.

The callback includes the `?code={}` query parameter that you can swap
for an access token:

``` bash
$ curl -X POST -u "client_id:secret" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=authorization_code -d code={code}
```

Want a more in-depth example? Check out our [authorization code grant sample app](https://bitbucket.org/atlassian/bb-cloud-auth-code-grant-sample-app).

### 2. Implicit Grant (4.2)

This flow is useful for browser-based apps that operate without server-side backends.

Request the end user for authorization by directing the browser to: `https://bitbucket.org/site/oauth2/authorize?client_id={client_id}&response_type=token`.

That will redirect to your preconfigured callback URL with a fragment
containing the access token
(`#access_token={token}&token_type=bearer`) where your page's JavaScript can
pull it out of the URL.

Want a more in-depth example? Check out our [implicit grant sample app](https://bitbucket.org/atlassian/bb-cloud-implicit-grant-sample-app).


### 3. Resource Owner Password Credentials Grant (4.3)

Useful if you have the end user's password but you want to use a more
secure end user access token instead. This method will not work when the user has two-step verification enabled. 

``` bash
$ curl -X POST -u "client_id:secret" \
  https://bitbucket.org/site/oauth2/access_token -d grant_type=password \
  -d username={username} -d password={password}
```

Want a more in-depth example? Check out our [resource owner password credentials grant sample app](https://bitbucket.org/atlassian/bb-cloud-resource-owner-grant-sample-app).

### 4. Client Credentials Grant (4.4)

Somewhat like our existing "2-LO" flow for OAuth 1. Obtain an access
token that represents not an end user, but the owner of the
client/consumer:

``` bash
$ curl -X POST -u "client_id:secret" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=client_credentials
```

Want a more in-depth example? Check out our [client credentials grant sample app](https://bitbucket.org/atlassian/bb-cloud-client-creds-grant-sample-app).

### 5. Bitbucket Cloud JWT Grant (urn:bitbucket:oauth2:jwt)

If your Atlassian Connect app uses JWT authentication, you can swap a 
JWT for an OAuth access token. The resulting access token represents the 
account for which the app is installed.

Make sure you send the JWT token in the Authorization request header
using the "JWT" scheme (case sensitive). Note that this custom scheme
makes this different from HTTP Basic Auth (and so you cannot use "curl
-u").

``` bash
$ curl -X POST -H "Authorization: JWT {jwt_token}" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=urn:bitbucket:oauth2:jwt
```

Want a more in-depth example? Check out our [bitbucket cloud JWT grant sample app](https://bitbucket.org/atlassian/bb-cloud-jwt-grant-sample-app).

## Making Requests

Once you have an access token, as per RFC-6750, you can use it in a request in any of
the following ways (in decreasing order of desirability):

1. Send it in a request header: `Authorization: Bearer {access_token}`
2. Include it in a (application/x-www-form-urlencoded) POST body as `access_token={access_token}`
3. Put in the query string of a non-POST: `?access_token={access_token}`


## Repository Cloning

Since apps will not be able to upload their own SSH keys to clone
with, access tokens can be used as Basic HTTP Auth credentials to
clone securely over HTTPS. This is much like GitHub, yet slightly
different:

``` bash
$ git clone https://x-token-auth:{access_token}@bitbucket.org/user/repo.git
```

The literal string `x-token-auth` as a substitute for username is
required (note the difference with GitHub who put the actual token in
the username field).


## Refresh Tokens

Our access tokens expire in one hour. When this happens you'll get 401
responses.

Most access token grant responses (Implicit and JWT excluded) therefore include a
refresh token that can then be used to generate a new access token,
without the need for end user participation:

``` bash
$ curl -X POST -u "client_id:secret" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=refresh_token -d refresh_token={refresh_token}
```

## Scopes

Scopes are defined on the client/consumer instance. Bitbucket Cloud does not
currently support the use of the optional scope parameter on the
individual grant requests.

When the scope parameter is provided, Bitbucket will validate that it
contains no scopes that were not already present on the client/consumer and
fail if additional scopes are requested, but asking for fewer scopes
will not affect the resulting access token.
