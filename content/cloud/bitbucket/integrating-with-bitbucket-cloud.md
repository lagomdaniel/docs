---
title: "Integrating with Bitbucket Cloud"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: intro
date: "2015-09-17"
---

# Integrating with Bitbucket Cloud

## Options for integrating with Bitbucket Cloud

You have three main options for integrating with Bitbucket Cloud:

1. Call the [REST API](https://confluence.atlassian.com/x/IYBGDQ) using REST command line tools, such as [curl](https://curl.haxx.se/) to automate operations in Bitbucket Cloud not otherwise available through the user interface.
2. Develop an Atlassian Connect app that integrates in the Bitbucket Cloud UI to add contextual information or funcitions (this page).
3. [Develop a Pipe for Bitbucket Pipelines](https://confluence.atlassian.com/x/2MWUOQ) that can integrate with an external system to deploy code, scan your code, call other services or implement other functionality.

## What is Atlassian Connect?

You can use the Atlassian Connect for Bitbucket Cloud to build apps which can connect with the Bitbucket UI and your own application set. An app could be an integration with another existing service, 
new features for the Atlassian application, or even a new product that runs within the Atlassian application.

## What is an Atlassian Connect app?

Simply understood, Atlassian Connect apps are web applications.
Atlassian Connect apps operate remotely over HTTP and can be written with any programming
language and web framework.

Fundamentally, Atlassian Connect apps have three major capabilities:

1. Insert content in [certain defined places](/cloud/bitbucket/app-descriptor) in the Atlassian application's UI.
2. Make calls to the Atlassian application's [REST API](https://confluence.atlassian.com/x/IYBGDQ).
3. Listen and respond to [WebHooks](https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html) fired by the Atlassian application.


## What does an Atlassian Connect app do?

- **Declare itself with a descriptor.** 
The [app descriptor](/cloud/bitbucket/app-descriptor/) is a JSON file that tells the Atlassian application about the app. 
In the descriptor, an app declares where it is hosted, 
which modules it intends to use, and which scopes it will need.
- **Extend the Atlassian application UI with modules.** 
The features that an app can use within the Atlassian application are
called modules. 
There are modules for [general pages](/cloud/bitbucket/modules/general-page/) in the application
or more specific locations, like [panels](/cloud/bitbucket/modules/web-item/) in Bitbucket Cloud.
- **Request appropriate scopes.** Your app must specify what type of access it needs from the Atlassian
application. You declare required scopes for the app in the app descriptor file. Scopes determine which REST API
resources the app can use.
- **Recognize the user.** Atlassian Connect apps [authenticate users](/cloud/bitbucket/understanding-jwt-for-apps/) via JWT, 
so each request from the Atlassian application to your app contains
details about the user currently viewing that page. 
This allows you to serve the right context,
respect necessary permissions, and make other decisions based on the user's identity.
- **Call the Atlassian application's REST API.** Your app can retrieve
data or push information to the Atlassian host application 
(for example, to build reports and create issues).
- **Respond to the Atlassian application's webhooks.** Your app can receive notifications
when certain events occur with [webhooks](https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html),
like when a Bitbucket pull request changes status. 
The webhook payload contains information about the
event, allowing your app to respond appropriately.


## How does Atlassian Connect work?

To an end user, your app should appear 
as a fully integrated part of the Atlassian application.
Once your app is registered with the application,
features are delivered from the UI and workflows of the host application. This deep level of integration is part of what makes 
Atlassian Connect apps so powerful.

<div id="architecture-graphic"></div>

### Architecture

Most Atlassian Connect apps are implemented as multi-tenanted services. This means that a
single Atlassian Connect app will support multiple subscribing Atlassian applications.

### Security

Security is critical in a distributed component model such as Atlassian Connect. Atlassian Connect relies on
HTTPS and JWT authentication to secure communication between your app, the Atlassian product, and the user.

Your apps actions are constrained by well-defined permissions. 
Your app can only perform activities it declares in its descriptor. 
These permissions are granted by Atlassian application administrators
when they install your app. Examples of permissions include
creating repositories, making commits (write permission), creating issues, and approving pull requests.  
These permissions help ensure the
security and stability of cloud instances.

Read our [security overview](/cloud/bitbucket/security-overview) for more details.

### Design

Since Atlassian Connect apps can insert content directly
into the Atlassian host application, it is critical that apps
are visually compatible with the Atlassian application's design.
To help developers, Atlassian's design team has created detailed 
[design guidelines](https://atlassian.design/)
and a [library of reusable front-end UI components](https://atlaskit.atlassian.com/).

## Let's get started

If you made it this far, you're ready to write your first Atlassian Connect app. See the [Getting started](/cloud/bitbucket/getting-started) guide to get set up and build your first app.
