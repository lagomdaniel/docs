---
title: "Deprecation notice - Bitbucket Cloud REST API version 1 is deprecated effective 30 June 2018"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2018-03-12"
---

# Deprecation notice - Bitbucket Cloud REST API version 1 is deprecated effective 30 June 2018

Atlassian is deprecating all Bitbucket Cloud's version 1.0 APIs to make way for more usable and maintainable alternatives in version 2.0. We heard from many of you that consistency was lacking with 1.0 of Bitbucket Cloud's API, and we're focusing on delivering a more consistent experience with this update. And with more exciting changes to come, you can expect more thoughtful design and uniformity moving forward.

With that in mind, as of June 30, 2018 the Bitbucket Cloud version 1.0 API is deprecated, and in accordance with the [Atlassian REST API policy](/platform/marketplace/atlassian-rest-api-policy/#deprecation-policy), **all version 1.0 APIs will be removed from Bitbucket Cloud's API permanently on 29 April 2019**.

## What will happen if I do nothing?

When the 1.0 APIs are removed on 29 April 2019, any apps or services still using `1.0` resources will not work. If you currently rely on a script, integration, or app that makes requests using `/1.0/` resources, you should update it to use a `/2.0/` alternative as soon as possible.

## <a name="deprecated-v1-api"></a>Deprecated APIs and 2.0 alternatives

These are all the 1.0 APIs that have been deprecated and their 2.0 alternatives. Not all functionality provided by the 1.0 APIs will be available in version 2.0. In the cases where there is not a direct 2.0 equivalent, refer to the sections below for details about the specific resources that are affected and for possible 2.0 alternatives.

| 1.0 resource                                     | 2.0 alternative                                             |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `account` (Removed)                              | [`user` (read-only)](/bitbucket/api/2/reference/search?q=user%2F)<br/> [`users`](/bitbucket/api/2/reference/search?q=users) <br/> [`teams`](/bitbucket/api/2/reference/resource/teams) |
| `changesets` (Removed)                           | [`commits`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commits) <br/> [`commit`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D) <br/>[`diffstat`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/diffstat/%7Bspec%7D) <br/>[`diff`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/diff/%7Bspec%7D) <br/> [`commit comments`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/comments) <br/>[`commit comment`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/comments/%7Bcomment_id%7D) |
| `consumers` (Removed)                            | None                                                        |
| `deploy-keys`                                    | [`deploy-keys`](/bitbucket/api/2/reference/resource/repositories/{username}/{repo_slug}/deploy-keys)  |
| `emails`                                         | [`emails`](/bitbucket/api/2/reference/search?q=emails)      |
| `events` (Removed)                               | [`hook_events`](/bitbucket/api/2/reference/search?q=hook_events)<br/>[`hooks`](/bitbucket/api/2/reference/search?q=hook)<br/>[webhooks module](/cloud/bitbucket/modules/webhook/) |
| `followers`                                      | [`watchers`](/bitbucket/api/2/reference/search?q=watch)     |
| `groups` (Removed)<br/>`group-privileges` (Removed) | None - [See Below](/cloud/bitbucket/deprecation-notice-v1-apis/#groups)  |
| `invitations` (Removed)                          | None                                                        |
| `issues`                                         | [`issues`](/bitbucket/api/2/reference/search?q=issue_tracker) |
| `links`  (Removed)                               | [linker module](/cloud/bitbucket/modules/linker/) <br/>[linker APIs](/bitbucket/api/2/reference/search?q=linkers) |
| `privileges` endpoint (Removed) <br/>`privileges` resource (Removed) | [2.0 permissions resources (read-only)](/bitbucket/api/2/reference/search?q=permissions) |
| `pullrequest comments`                           | [`pullrequest comments`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/pullrequests/%7Bpull_request_id%7D/comments) |
| `repositories`                                   | [`repositories`](/bitbucket/api/2/reference/search?q=repositories) |
| `services`  (Removed)                            | [`hook_events`](/bitbucket/api/2/reference/search?q=hook_events)<br/>[`hooks`](/bitbucket/api/2/reference/search?q=hook)<br/>[webhooks module](/cloud/bitbucket/modules/webhook/) |
| `src`                                            | [`src`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/src/%7Bnode%7D/%7Bpath%7D) |
| `ssh-keys`                                       | [`ssh-keys`](/bitbucket/api/2/reference/search?q=ssh-keys)  |
| `user` <br/> `users`                             | [`user` (read-only)](/bitbucket/api/2/reference/search?q=user%2F)<br/> [`users`](/bitbucket/api/2/reference/search?q=users) |
| `wiki`  (Removed)                                | None                                                        |

## Significant changes and removed resources

The semantics of these 1.0 APIs were changed in a significant way or will not be included in 2.0:

- [Account: User/Users/Teams](/cloud/bitbucket/deprecation-notice-v1-apis/#account)
- [Changesets: Commits](/cloud/bitbucket/deprecation-notice-v1-apis/#changesets)
- [Deploy keys: None](/cloud/bitbucket/deprecation-notice-v1-apis/#deploykeys)
- [Events/Services: Webhooks](/cloud/bitbucket/deprecation-notice-v1-apis/#events)
- [Followers: Watchers](/cloud/bitbucket/deprecation-notice-v1-apis/#followers)
- [Groups: None](/cloud/bitbucket/deprecation-notice-v1-apis/#groups)
- [Invitations: None](/cloud/bitbucket/deprecation-notice-v1-apis/#invitations)
- [Links: Linkers](/cloud/bitbucket/deprecation-notice-v1-apis/#links)
- [Privileges: Permissions](/cloud/bitbucket/deprecation-notice-v1-apis/#privileges)
- [Oauth/Consumers: None](/cloud/bitbucket/deprecation-notice-v1-apis/#oauth)
- [User: Read-only](/cloud/bitbucket/deprecation-notice-v1-apis/#user)
- [Wiki: None](/cloud/bitbucket/deprecation-notice-v1-apis/#wiki)

### <a name="account"></a>Account: User/Users/Teams

The `/1.0/users/{accountname}` resource could be used to return a user object, which could also be a team, but is not available in 2.0. Use  `2.0/users/{uuid|account_id}` or `/2.0/teams/{username|uuid}` instead.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `account` (Removed)                              | [`user` (read-only)](/bitbucket/api/2/reference/search?q=user%2F)<br/> [`users`](/bitbucket/api/2/reference/search?q=users) <br/> [`teams`](/bitbucket/api/2/reference/resource/teams) |

### <a name="changesets"></a>Changesets: Commits

Changesets endpoints are not available in 2.0. Use the corresponding 2.0 APIs instead.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `changesets`     | [`commits`](bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commits) <br/> [`commit`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D) <br/>  [`diffstat`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/diffstat/%7Bspec%7D) <br/> [`diff`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/diff/%7Bspec%7D) <br/> [`commit comments`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/comments) <br/> [`commit comment`](/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/comments/%7Bcomment_id%7D) <br/>|


### <a name="events"></a>Events/Services: Webhooks

In 1.0 we allowed your to poll for events using the `events` API, or by registering for events to be pushed to you using the `brokers` API. That functionality has been replaced in 2.0 by [webhooks](/cloud/bitbucket/modules/webhook/). Webhooks are also a module in Connect, so that apps can register to receive hooks for events using their app descriptor. We provide the `hook_events` APIs for querying which events are available to register for, and we provide the `hooks` APIs which allow users to query for the current registrations and also to create new registrations.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `events` <br/>`services`                         | [`hook_events`](/bitbucket/api/2/reference/search?q=hook_events)<br/>[`hooks`](/bitbucket/api/2/reference/search?q=hook)<br/>[webhooks module](/cloud/bitbucket/modules/webhook/) |

### <a name="followers"></a>Followers: Watchers

In 1.0, the term "followers" for a user tracking changes to a repository or an issue and getting email updates. In 2.0 these are now called "watchers" instead, but the terms are synonymous. Use one of the 2.0 watchers APIs to read or edit the watchers of a repository.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `followers`       | [`watchers`](/bitbucket/api/2/reference/search?q=watch) |

### <a name="groups"></a>Groups: None

In 1.0, we allowed you to manage your groups in Bitbucket using the `groups` API. To comply with [GDPR restrictions](https://developer.atlassian.com/cloud/bitbucket/bitbucket-api-changes-gdpr/) on personally identifiable information, and to adopt the [Atlassian account](https://confluence.atlassian.com/cloud/atlassian-account-for-users-873871199.html), these endpoints do not have a direct replacement in the 2.0 API. Moving forward with the 2.0 REST API and beyond we intend to rely on the [Atlassian Cloud Admin API](https://developer.atlassian.com/cloud/admin/organization) for user and group management, but those API endpoints are not yet available. 

Until the Atlassian platform services are fully available in Bitbucket we are planning to continue support for the following 1.0 REST endpoints: 

* `/1.0/groups`
* `/1.0/groups/{accountname}/`
* `/1.0/groups/{accountname}/{group_slug}/`
* `/1.0/groups/{accountname}/{group_slug}/members`
* `/1.0/groups/{accountname}/{group_slug}/members/{membername}`
* `/1.0/group-privileges/{accountname}`
* `/1.0/group-privileges/{accountname}/{repo_slug}`
* `/1.0/group-privileges/{accountname}/{repo_slug}/{group_owner}/{group_slug}`
* `/1.0/group-privileges/{accountname}/{group_owner}/{group_slug}`

These endpoints and all other 1.0 and 2.0 endpoints will become GDPR-compliant in April 2019. Please refer to the [user privacy migration guideline](https://developer.atlassian.com/cloud/bitbucket/bbc-gdpr-api-migration-guide/) for specific changes in the payload returned by these APIs.

### <a name="invitations"></a>Invitations: None

You won't be able to send invitations to Bitbucket with the 2.0 REST API.

### <a name="links"></a>Links: Linkers

The 1.0 `links` resource supplied functionality for adding, updating, removing, and viewing links associated with your repositories. There is currently no way to do this with the 2.0 APIs. However, if you're making a Connect app, check out the [linker module](/cloud/bitbucket/modules/linker/) and corresponding [linker APIs](/bitbucket/api/2/reference/search?q=linkers) to configure linkers with Connect app descriptors.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `links`                                          | [linker module](/cloud/bitbucket/modules/linker/) <br/>[linker APIs](/bitbucket/api/2/reference/search?q=linkers) |

### <a name="privileges"></a>Privileges: Permissions

User access to repositories can no longer be managed directly with the REST API. However you can still GET details of repository and account permissions.

| 1.0 resource                                      | 2.0 alternative                                              |
| ------------------------------------------------ | ----------------------------------------------------------- |
| `privileges` endpoint <br/> `privileges` resource | [2.0 permissions resources](/bitbucket/api/2/reference/search?q=permissions)|

### <a name="oauth"></a>Oauth/Consumers: None

The 1.0 APIs offered a way to query and update Oauth configuration. This functionality does not exist in 2.0 as part of an effort to expose less permissions-based logic in the REST API.

### <a name="user"></a>User: Read-only

The 1.0 `user` endpoint could be used to retrieve or modify information related to the currently authenticated Bitbucket Cloud user. In 2.0 the `user` endpoint is read-only; you can no longer modify the currently logged in user with the API. Also, where 1.0 included a repositories list, 2.0 has a link property for repositories.

### <a name="wiki"></a>Wiki: None

Bitbucket wikis can no longer be created or updated using the API. We will consider adding the endpoint to 2.0 in the future.
