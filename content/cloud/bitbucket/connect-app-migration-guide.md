---
title: "Atlassian Connect App migration guide to improve user privacy"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2018-11-28"
---
{{< include path="docs/content/cloud/connect/guides/connect-app-gdpr-migrations-header.snippet.md">}}

{{% note %}}
### Some changes are still in development

We always strive to give our vendors as much time as possible to plan and do the required work. This is why we have published this migration guide before all changes have been released into production.
See [APIs affected by GDPR API Migration](../api-changes-for-user-privacy-announcement-connect/) for the detailed list and implementation status of APIs changes.
*It is advised to not enable the Connect GDPR API migration for existing Marketplace-listed production versions. We will continue to add API changes under the API migration opt-in, so apps may break. Check this page to find out when it is safe to enable the API migration for your customers.*

Need help? Head for the developer community for [Bitbucket Cloud](https://community.developer.atlassian.com/t/bitbucket-cloud-api-privacy-updates-deprecation-notice-12-oct-2018/24168).
{{% /note %}}

{{< include path="docs/content/cloud/connect/guides/connect-app-gdpr-migrations.snippet.md">}}
