---
title: "Bitbucket Cloud REST APIs are changing to improve user privacy"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2019-04-12"
---

# Bitbucket Cloud REST APIs are changing to improve user privacy

In order to improve user privacy in accordance with GDPR regulations, Bitbucket and other Atlassian Cloud products are
updating our product APIs to consolidate how we manage access to personal data. These API changes are necessary to support
upcoming improvements to Atlassian products that give users greater control over who can see and access their data.

The short summary is that username endpoints and username fields will stop being available on **29 April 2019**,
and we're introducing some new data points that are available immediately to replace them.

## Introducing Atlassian Account ID and Nicknames

In all cases where Bitbucket APIs return user details, we are introducing two new fields to the object body,
Atlassian account ID `account_id` and `nickname`.

Atlassian account IDs are a unique identifier for every  Atlassian account user and should be considered the
primary key for interacting with users via Atlassian APIs. Account IDs are shared across all Atlassian cloud products, and will remain
as a stable reference for users for the future.

`Nickname` will serve as a non-unique user identifier substituting for username where a human readable user-reference is desired, and
in conjunction with Atlassian account ID, will be available for use in most user objects.

Note that while we are introducing a nickname field, it will not be effective for identifying user accounts, because nicknames
are not guaranteed to be globally unique. If you do store user data, we strongly encourage you to use Atlassian account IDs as the
identifier for your data.

## Removal of usernames from user-referencing APIs

We are deprecating the use of `username` from user objects through the API. This affects
URLs containing a username in the path on `/users` endpoints. As of 29 April 2019 usernames will no longer be supported in API URLs that
reference users, and account_id or uuid should be used to identify an account instead

(for example, `/2.0/users/557058:c0b72ad0-1cb5-4018-9cdc-0cde8492c443` vs `/2.0/users/evzijst`)

We are excluding repo URLs from this deprecation to avoid breaking repo references. As part of giving users better control of their data,
we are going to separate references to user-spaces from content-spaces, which will preserve existing repo URLs.
For more information on this, see [Change notice: Updates to Bitbucket Cloud repository URL paths](/cloud/bitbucket/bitbucket-api-changes-workspaces).


### Replace username fields with Atlassian Account ID (account_id)

The Bitbucket's user object schema will lose its username field after 29 April 2019. This will
affect clients that use username to identify accounts in API request objects.

For instance, instead of:

``` bash
$ curl https://api.bitbucket.org/2.0/repositories/foo/bar/forks \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "my_fork",
    "owner": {
      "username": "evzijst"
    }
}'
```

do this instead:

``` bash
$ curl https://api.bitbucket.org/2.0/repositories/foo/bar/forks \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "my_fork",
    "owner": {
      "account_id": "557058:c0b72ad0-1cb5-4018-9cdc-0cde8492c443"
    }
}'
```

The exception to the removal of usernames is the `/2.0/user` endpoint, which will continue to contain the `username` field.
This endpoint only ever returns the authenticated user's own user object, not that of other users.

## Affected API endpoints

The following API endpoints will change or are part of this overall change:

<table>
	<thead>
		<tr>
			<td>Path</td>
			<td>Methods</td>
		    <td>How will it change?</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a href="https://developer.atlassian.com/bitbucket/api/2/reference/resource/users/%7Busername%7D/followers">/users/{username|uuid}/followers</a></td>
			<td>Get</td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><a href="https://developer.atlassian.com/bitbucket/api/2/reference/resource/users/%7Busername%7D/following">/users/{username|uuid}/following</a></td>
			<td>Get</td>
			<td>Removed 29 April 2019</td>
		</tr>
	</tbody>
</table>

## Changes to Bitbucket user objects

When a user object is returned in a Bitbucket API today, either through the normal REST endpoints or through webhooks,
it includes the field `display_name`, as well as a link to the user's avatar (`links.avatar.href`) These
fields will change to accommodate new privacy controls which grant users authority over how their information is
displayed to others. The new privacy controls will allow users with an Atlassian ID account to control what pieces of
data are available to various audiences. This means that depending on the relationship between the requesting user and
the user data returned in the request, the value of `display_name` and `links.avatar.href` may be different depending on the privacy controls set
by the user. In situations where an app or Connect application is making the request, that is when there is no single
Atlassian Account making the request, the value of these fields will be different. However, `display_name` and `links.avatar.href`
will always be present and have a value (possibly an empty string in the case of `display_name`).

### User object

The following `user` fields will change substantially or are part of the changes:

<table>
	<thead>
		<tr>
			<td>Field</td>
			<td>Value</td>
			<td>How will it change?</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>account_id</code></td>
			<td>"557057:b8796e61-5372-3e09-af7c-cb28aadb72b3"</td>
			<td></td>
		</tr>
		<tr>
			<td><code>created_on</code></td>
			<td>"2016-08-31T18:27:07+00:00"</td>
			<td></td>
		</tr>
		<tr>
			<td><code>display_name</code></td>
			<td>"Bitbucket User"</td>
			<td>The value of this field will change depending on the privacy settings of the user account and the relationship between the requesting user and the target user. This field will always have some value.
			</td>
		</tr>
		<tr>
			<td><code>links.avatar.href</code></td>
			<td>https://bitbucket.org/account/{username}/avatar/</td>
			<td>The actual URL returned here should be considered opaque.</td>
		</tr>
		<tr>
			<td><code>links.followers.href</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>links.following.href</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>links.hooks.href</code></td>
			<td>https://api.bitbucket.org/2.0/users/{username}/hooks</td>
			<td>https://api.bitbucket.org/2.0/users/{account_id}/hooks</td>
		</tr>
		<tr>
			<td><code>links.html.href</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>links.self.href</code></td>
			<td>https://api.bitbucket.org/2.0/users/{username}</td>
			<td>https://api.bitbucket.org/2.0/users/{uuid|account_id}</td>
		</tr>
		<tr>
			<td><code>links.snippets.href</code></td>
			<td>https://api.bitbucket.org/2.0/snippets/{username}</td>
			<td>https://api.bitbucket.org/2.0/snippets/{uuid|account_id}</td>
		</tr>
		<tr>
			<td><code>location</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>type</code></td>
			<td><ul>
					<li>"team"</li>
					<li>"user"</li>
				</ul>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><code>username</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>uuid</code></td>
			<td>{33e297d1-567f-4c32-89b4-4b2759c5f5ae}</td>
			<td></td>
		</tr>
		<tr>
			<td><code>website</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>nickname</code></td>
			<td>"Bitbucket user"</td>
			<td><em>New</em> A new user identifier which is easily customized and always publicly visible. This user identifier will not be required to be unique to that user. </td>
		</tr>
		<tr>
			<td><code>account_status</code></td>
			<td>
				<ul>
					<li>"active"</li>
					<li>"inactive"</li>
					<li>"closed"</li>
				</ul>
			</td>
			<td><em>New</em> When user accounts are closed, their records remain for purposes including audit logs and @mentions. This field is used to distinguish between those and active accounts.
			</td>
		</tr>
	</tbody>
</table>

## Changes to the issue import and export

The issue importer and exporter currently serializes username as the reference to the user object on the following fields:

* `reporter`
* `assignee`
* `watchers`
* `voters`
* `meta.default_assignee`
* `log.user`
* `attachments.user`
* `comments.user`

In the new 2.0 version of the exporter, `username` will be removed from these fields and replaced with an object with two fields:

``` text
{ display_name: "Sean McUser", account_id: "557057:b8796e61-5372-3e09-af7c-cb28aadb72b3" }
```

From 12 October 2018 to 29 April 2019, the zip archives will contain both the legacy `db-1.0.json` file, as well as a new `db-2.0.json` file. The latter uses the new user representation without username.
After 29 April 2019, we will only emit 2.0 and only allow imports of 2.0 archives.

## Changes to the issue changes API

Example: `https://api.bitbucket.org/2.0/repositories/testingtheteammodel/foobar/issues/1/changes/`

The issue change API can return any changes that have been made to an issue object. If the assignee of an issue has
changed, currently that field just returns the usernames of the previous and current assignee, for example.

``` json
"changes": {
    "assignee": {
        "new": "jarredcolli",
        "old": "smccullough"
    }
},
```

Going forward, a new field `assignee_account_id` is added that contains the user's account_id. After 29 April 2019, the `assignee` field will disappear.

``` json
"changes": {
    "assignee_account_id": {
        "new": "557057:b8796e61-5372-3e09-af7c-cb28aadb72b3",
        "old": "6:a3791e61-2526-7e01-bc6c-1a49ce3172c9"
    }
},
```

## Changes to querying

[Querying and filtering](https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering) by username will be deprecated, and after 29 April 2019,
you will not be able to use a `username` field as a BBQL query field anymore (e.g. `reporter.username = "evzijst"` or `state = "OPEN" AND reviewers.username = "bgummer"`).

In order to filter query responses by users, instead of querying against the `username` field, you can query against the existing `account_id` and `uuid` fields.
When ids are not readily available or appropriate, you can query against the new `nickname` field (for example, `reporter.nickname = "evzijst"`).

Thank you for building with Bitbucket Cloud, follow up and discuss in the Atlassian Community:

* For teams building apps in [Bitbucket Cloud](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud)
* For general discussions about the [Bitbucket Cloud API](https://community.atlassian.com/t5/forums/searchpage/tab/message?advanced=false&allow_punctuation=false&q=Bitbucket+API)
