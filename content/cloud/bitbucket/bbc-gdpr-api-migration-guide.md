---
title: "REST API migration guide - privacy related API changes"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2018-03-12"
---

# Migration guide for major changes to Bitbucket Cloud REST APIs to improve user privacy

#### The deprecation period for these changes began on 01 October 2018.

This guide shows you how to update your app or integration to adopt the GDPR-related changes for the Bitbucket Cloud
REST APIs. It describes how the user privacy improvements affect the REST APIs, provides a migration path to the new
REST APIs, and lists all of the REST API changes.

## Overview

As [previously announced](https://developer.atlassian.com/cloud/bitbucket/bitbucket-api-changes-gdpr/), Atlassian is
making a number of changes to our products and APIs to improve user privacy in accordance with the [European General
Data Protection Regulation
(GDPR)](https://www.atlassian.com/blog/announcements/atlassian-and-gdpr-our-commitment-to-data-privacy/).

For Bitbucket Cloud, these changes include:

- **Changes to how users are identified**: Personal data that is used to identify users, such as the `username` will be
    removed from the REST APIs. Users will be identified by their Atlassian account ID (`account_id`) instead. You can also continue to use the `uuid` for the user account.

- **Changes to the visibility of user information**: Users will be able to restrict the visibility of their personal
    data through their user profile privacy settings, or in the case of a managed account, the visibility settings that
    are decided by the site administrator. This means that fields such as `display_name` will return different values
    based off the privacy settings of the target user and the relationship between the requesting user and the target
    user.

- **Changes to Atlassian Connect APIs**: All personal data will be removed from the Connect REST APIs and replaced with
	`accountId` where appropriate. For details on migrating Connect apps to the GDPR-compliant version of the Connect REST
	APIs, see the [Connect migration guide](../connect-app-migration-guide/).

## Summary of changes

Below is a high-level summary of the REST API changes:

- Requests that use `username` to identify accounts in API request objects will now use the `account_id`

- Some fields in the user object returned by our API, e.g.  `display_name` and `nickname`, will return different data
    based on the users settings. This can result in some fields returning the same value, e.g. in some circumstances
    `nickname` and `display_name` will be the same in cases that the user has set `display_name` visibility to
    the most restrictive setting.

- The addition of the `nickname` field to retrieve a non-unique human readable user reference.

- Use the `account_id` field to identify user accounts or as a path parameter ( for example:`/2.0/users/{account_id}`) . Do not use the nickname field as it will return non-unique results

- The issue importer and exporter will use the `display_name` and `account_id` fields to reference user information in the
    JSON data document inside the generated archive files.


- The [issue change
    API](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues/%7Bissue_id%7D/changes) will use the `assignee_account_id` field (which contains the `account_id` of the user).

- To [filter query responses](https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering) by users you
    can query against the existing `account_id` and `uuid` fields.


## Changes to Bitbucket user objects

Currently, the data in the user object includes the `display_name` field and the `links.avatar.href` field which provides an avatar. That response will change as we introduce new privacy settings to give greater control over what data a user shares.

The response for either value (`display_name` or `links.avatar.href`) will depend on the privacy settings of the associated account. Both fields will always return a value, in rare cases that value might be an empty string.

### User object

The following `user` fields will change or are part of the changes we are implementing:

<table>
	<thead>
		<tr>
			<td>Field</td>
			<td>Value</td>
			<td>How will it change?</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>account_id</code></td>
			<td>"557057:b8796e61-5372-3e09-af7c-cb28aadb72b3"</td>
			<td>No change</td>
		</tr>
		<tr>
			<td><code>created_on</code></td>
			<td>"2016-08-31T18:27:07+00:00"</td>
			<td>No change</td>
		</tr>
		<tr>
			<td><code>display_name</code></td>
			<td>"Bitbucket User"</td>
      <td>The value of this field will change depending on the privacy settings of the user account and the relationship
      between the requesting user and the target user. This field will always have some value.
			</td>
		</tr>
		<tr>
			<td><code>links.avatar.href</code></td>
			<td>https://bitbucket.org/account/{username}/avatar/</td>
			<td>The actual URL returned here should be considered opaque.</td>
		</tr>
		<tr>
			<td><code>links.followers.href</code></td>
			<td>Returns list of followers</td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>links.following.href</code></td>
			<td>Returns a list of users following the account</td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>links.hooks.href</code></td>
			<td>https://api.bitbucket.org/2.0/users/{username}/hooks</td>
			<td>https://api.bitbucket.org/2.0/users/{account_id}/hooks</td>
		</tr>
		<tr>
			<td><code>links.html.href</code></td>
			<td>returns the user's profile link</td>
			<td>username in url will be replaced by Bitbucket UUID</td>
		</tr>
		<tr>
			<td><code>links.repositories.href</code></td>
			<td>https://api.bitbucket.org/2.0/repositories/{username}</td>
			<td>https://api.bitbucket.org/2.0/repositories/{uuid|account_id}</td>
		</tr>
		<tr>
			<td><code>links.self.href</code></td>
			<td>https://api.bitbucket.org/2.0/users/{username}</td>
			<td>https://api.bitbucket.org/2.0/users/{uuid|account_id}</td>
		</tr>
		<tr>
			<td><code>links.snippets.href</code></td>
			<td>https://api.bitbucket.org/2.0/snippets/{username}</td>
			<td>https://api.bitbucket.org/2.0/snippets/{uuid|account_id}</td>
		</tr>
		<tr>
			<td><code>location</code></td>
			<td>Returns a location string</td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>type</code></td>
			<td><ul>
					<li>"team"</li>
					<li>"user"</li>
				</ul>
			</td>
			<td></td>
		</tr>
		<tr>
			<td><code>username</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>uuid</code></td>
			<td>{33e297d1-567f-4c32-89b4-4b2759c5f5ae}</td>
			<td></td>
		</tr>
		<tr>
			<td><code>website</code></td>
			<td></td>
			<td>Removed 29 April 2019</td>
		</tr>
		<tr>
			<td><code>nickname</code></td>
			<td>"Bitbucket user"</td>
      		<td><em>New</em> A new user identifier which is easily customized and always publicly visible. The <code>nickname</code> field:
      			<ul>
      				<li>Is part of Atlassian Identity and Atlassian Account</li>
      				<li>Is not unique to any user so duplicate nicknames can exist in different Atlassian Accounts.</li>
      			</ul>
      		</td>
		</tr>
		<tr>
			<td><code>account_status</code></td>
			<td>
				<ul>
					<li>"active"</li>
					<li>"inactive"</li>
					<li>"closed"</li>
				</ul>
			</td>
      <td><em>New</em> When user accounts are closed, their records remain for purposes including audit logs and
      @mentions. This field is used to distinguish between those and active accounts.
			</td>
		</tr>
	</tbody>
</table>


## Path parameter changes

For all REST API calls where you had `username` as a path parameter, switch to either the `uuid` or `account_id` of the user.

There are two exceptions to this:

 - The `/2.0/teams/` endpoint will continue to have a `username` field which references the teams name. You can also use the teams `uuid`.
 - The other exception to the removal of usernames is the `/2.0/user` endpoint. This endpoint only uses the `{username}` field to return the authenticated user's own user object.

<table>
   <thead>
      <tr>
         <td> Resource & Current Path Parameter </td>
         <td> New Path Parameter </td>
      </tr>
   </thead>
   <tr>
      <td> */{target_user}/*	 </td>
      <td> */{uuid|account_id}/* </td>
   </tr>
   <tr>
      <td> */{username}/*	</td>
      <td> */{uuid|account_id}/*	</td>
   </tr>
   <tr>
      <td> /2.0/teams/{username}	</td>
      <td> /2.0/teams/{username} (<b>The username of the team</b>.) </td>
   </tr>
   <tr>
      <td> /2.0/users/{username}/followers	</td>
      <td> Removed 29 April 2019	</td>
   </tr>
   <tr>
      <td> /2.0/users/{username}/following	</td>
      <td> Removed 29 April 2019	</td>
   </tr>
   <tr>
      <td> /2.0/users/{username}/members	</td>
      <td> Removed 29 April 2019	</td>
   </tr>
</table>


## addon
#### Resources
 - /2.0/addon/users/{target_user}/events/{event_key}

#### Methods
 - GET

#### Request query parameter changes
For addons owned by individual users rather than teams, replace `username` with either  `uuid` or `account_id`.

#### Response changes
None


## pullrequests

#### Resources
- /2.0/pullrequests/{target_user}

#### Methods
 - GET

#### Request query parameter changes
Instead of `username` use either `uuid` or `account_id`

#### Response changes
Embedded user objects follow the schema changes below.


## repositories

#### Resources

 - /2.0/repositories/{username}
 - /2.0/repositories/{username}/{repo_slug}

#### Methods

To determine the available method see the [resource list](https://developer.atlassian.com/bitbucket/api/2/reference/search?q=%2Frepositories%2F).

#### Request query parameter changes
For repositories owned by individual users rather than teams, replace `username` with either  `uuid` or `account_id`.

#### Response changes
Embedded user objects will follow the schema changes below.

## snippets

#### Resources
 - /snippets
 - /snippets/{username}
 - /snippets/{username}/{encoded_id}
 - /snippets/{username}/{encoded_id}/comments
 - /snippets/{username}/{encoded_id}/comments/{comment_id}

#### Methods

To determine the available method, see the [resource list](https://developer.atlassian.com/bitbucket/api/2/reference/search?q=%2Fsnippets%2F)

#### Request query parameter changes
Instead of `username` use either `uuid` or `account_id`

#### Response changes
Embedded user objects will follow the schema changes below.


## user

#### Resources

 - /2.0/user

#### Methods

- GET

#### Request query parameter changes
No query parameter available

#### Response changes
Embedded user objects will follow the schema changes below, except that `username` will still be present. This field can
be used as the username for authentication methods.

## users

#### Resources

 - /2.0/users/{username}
 - /2.0/users/{username}/*

#### Methods

To determine the available method, see the [resource list](https://developer.atlassian.com/bitbucket/api/2/reference/search?q=%2Fusers%2F%7Busername%7D)

#### Request query parameter changes
Instead of `username` use either `uuid` or `account_id`

#### Response changes
Embedded user objects will follow the schema changes below.



## Changes to the issue import and export

The issue importer and exporter currently serializes username as the reference to the user object on the following
fields:

* `reporter`
* `assignee`
* `watchers`
* `voters`
* `meta.default_assignee`
* `log.user`
* `attachments.user`
* `comments.user`

In the new 2.0 version of the exporter, `username` will be removed from these fields and replaced with an object with
two fields:

``` text
{ display_name: "Sean McUser", account_id: "557057:b8796e61-5372-3e09-af7c-cb28aadb72b3" }
```

From 12 October 2018 to 29 April 2019, the zip archives will contain both the legacy `db-1.0.json` file, as well as a
new `db-2.0.json` file. The latter uses the new user representation without username.
After 29 April 2019, we will only emit 2.0 and only allow imports of 2.0 archives.

## Changes to the issue changes API

Example: https://api.bitbucket.org/2.0/repositories/testingtheteammodel/foobar/issues/1/changes/

The issue change API can return any changes that have been made to an issue object. If the assignee of an issue has
changed, currently that field just returns the usernames of the previous and current assignee, for example.

``` json
"changes": {
    "assignee": {
        "new": "jarredcolli",
        "old": "smccullough"
    }
},
```

Going forward, a new field `assignee_account_id` is added that contains the user's account_id. After 29 April 2019, the
`assignee` field will disappear.

``` json
"changes": {
    "assignee_account_id": {
        "new": "557057:b8796e61-5372-3e09-af7c-cb28aadb72b3",
        "old": "6:a3791e61-2526-7e01-bc6c-1a49ce3172c9"
    }
},
```

## Changes to querying

Beginning 29 April 2019 you will no longer be able to use `username` as a field in Bitbucket API [queries or filters](https://developer.atlassian.com/bitbucket/api/2/reference/meta/filtering) like  `reporter.username = "evzijst"` or `state = "OPEN" AND reviewers.username = "bgummer"`.

Instead of `username` use the `account_id` and `uuid` fields to filter responses by user. You can query against the new nickname field when no other id is available (example: `reporter.nickname = "evzijst"`). **Note**: The `nickname` field is not a unique result. Many users can have the same nickname.

Thank you for building with Bitbucket Cloud, follow up and discuss in the Atlassian Community:

* For teams building apps in [Bitbucket
 Cloud](https://community.developer.atlassian.com/c/bitbucket-development/bitbucket-cloud) For general discussions
* about the [Bitbucket Cloud
 API](https://community.atlassian.com/t5/forums/searchpage/tab/message?advanced=false&allow_punctuation=false&q=Bitbucket+API)


## About the V1 APIs

Bitbucket's V1 APIs will be removed on **April 29th, 2019**. If you are still using the V1 APIs in your application, please
refer to the [deprecation notice](https://developer.atlassian.com/cloud/bitbucket/deprecation-notice-v1-apis) for more
information on how to migrate to the new APIs.
