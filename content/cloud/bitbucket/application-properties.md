---
title: "Application properties"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning
date: "2019-03-04"
---

# Application properties

Application properties provide a way for Connect apps to store data inside Bitbucket,
as if it were a key/value store. We support storing properties against repositories,
users, pull requests, and commits. Properties stored against other object
types will likely be supported in the future.

The URL for fetching a property value looks like this:

```json
GET https://api.bitbucket.org/2.0/repositories/<account>/<repository>/properties/<app key>/<property>
```

In this example, <code>account</code> is the account where the app is installed, <code>repository</code> is the
repository against which the property is stored, <code>app key</code> is the key of the application,
and <code>property</code> is the name of the property.

Set a property value with this URL:

```json
PUT https://api.bitbucket.org/2.0/repositories/<account>/<repository>/properties/<app key>/<property>

{
  'foo': 'bar'
}
```

All the placeholders in this <code>PUT</code>
example are the same as for the <code>GET</code> example, and additionally there
is a JSON body containing the value being set for the property.

There is a corresponding <code>DELETE</code> method available to remove properties that
were previously set:

```json
DELETE https://api.bitbucket.org/2.0/repositories/<account>/<repository>/properties/<app key>/<property>
```

### Authentication

To <code>GET</code> a property, any valid Bitbucket API
authentication type may be used (see [this page](/bitbucket/api/2/reference/meta/authentication)
and [this page](/cloud/bitbucket/authentication-for-apps/) for ways to authenticate
requests to Bitbucket). A property may not be visible
to requests signed with certain kinds of auth depending on the property
attributes (explained below).

To <code>PUT</code> or <code>DELETE</code> a property,
requests may always be made
[using a JWT token](/cloud/bitbucket/understanding-jwt-for-apps).
Additionally, they may sometimes
be made from the app client in the browser using the
[AP.require('request', ...)](/cloud/bitbucket/jsapi/request/)
pattern. The situations where it is appropriate
to call the mutative methods from the browser are described in the
following section on Attributes.

### Attributes

When you create a property value you can set attributes for it, like this:

```json
PUT https://api.bitbucket.org/2.0/repositories/<account>/<repository>/properties/<app key>/<property>

{
  'foo': 'bar',
  '_attributes': [
    'read_only',
    'public'
  ]
}
```

The two supported attributes are <code>read_only</code> and <code>public</code>.

Properties with the <code>read_only</code> attribute are only able to be
set or removed using JWT authentication.
This provides a way for apps to set values that
cannot be tampered with by an end user that invokes an
<code>AP.require('request', ...)</code> call to the properties API from
the browser. This way, an app knows that it is the only one that has
mutated these values, which could be handy if the property is being used
to enforce security controls. Properties without this attribute may also be
mutated via calls to <code>AP.require('request', ...)</code> from the
browser.

Properties with <code>public</code> attribute may be queried by callers
other than the app, providing a way for apps to share non-sensitive information
externally with others. If this attribute is not present, a property
may only be queried by the app that created it, either with a JWT-signed
request or via the <code>AP.require('request', ...)</code> API called
from the app browser client.

### Using properties in descriptor conditions

It is possible to refer to application properties in the
app descriptor, in order to configure the conditional render logic
of a module, such as in the following example:

```json
"conditions": [{
  "type": "or",
  "conditions": [{
    "condition": "property_exists",
    "target": "repository.properties",
    "params": {
      "appKey": {
        "eval": "app.key"
      },
      "objectName": "myproperty",
      "propertyKey": "enabled"
    },
    "invert": true
  },
  {
    "condition": "equals",
    "target": "repository.properties.get(attribute=app.key).myproperty.enabled",
    "params": {
      "value": "true"
    }
  }]
}]
```

In this example of a module [conditions block](/cloud/bitbucket/conditions/)
the module is only rendered if either the property <code>myproperty</code>
has not been set, or if has been set to the value:

```json
{
  'enabled': true
}
```

If it has been set to the value:

```json
{
  'enabled': false
}
```

or any other value for <code>enabled</code> other than true,
the module will not be shown in the UI for this account/repository.

### Accessing property values via the API

Another cool thing you can do is request that repository properties
be returned in API responses that are returning Bitbucket
repositories. To request properties are attached to an API
response you can do:

```json
curl -G -u <user>:<app password> https://api.bitbucket.org/2.0/repositories/<account>/<repository> \
--data-urlencode 'fields=+properties.*'
```

If you have property values containing nested objects you'd like returned
in the response, you will need to appropriately specify the depth of the
inclusion of properties field (for example, `fields=+properties.*.*.*`).

Note, non-public properties will only be visible
to requests signed with a JWT token crafted by the app, or if an app
makes requests using <code>AP.require('request', ...)</code> from within
a connect iframe. Public properties
can be accessed via the API by any requests that have permission to
access the repository.
