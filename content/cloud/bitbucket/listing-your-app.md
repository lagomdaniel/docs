---
title: "Listing your app"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning
learning: guides 
date: "2017-03-28"
---

# Listing your app 

Learn how to submit your app for approval. Once it's approved we'll list it in both the [Atlassian Marketplace](https://marketplace.atlassian.com) and the [add-on directory](https://bitbucket.org/account/addon-directory) inside Bitbucket Cloud. 

### Create an Atlassian account

To submit an app you'll need an Atlassian account and a Marketplace vendor identity. 
If you've already got both of those, skip to the next section. You're all set!

To set up an Atlassian account and a Marketplace vendor identity do the following: 

1. Go to [Atlassian Marketplace]( https://marketplace.atlassian.com/login). 
2. Log in using your Atlassian account credentials or click **Sign up** and create an account.
   Once you create an account you'll be redirected to the Atlassian Marketplace. 
3. Go to [Create vendor](https://marketplace.atlassian.com/manage/vendor/create). Complete and submit the form. 

### Create your listing 

All apps are submitted as private apps. When you're ready you can submit your app for approval, we'll review it, and make it public when it's ready. 
You'll need details from your descriptor like your <code>key</code>, <code>name</code>, and <code>baseUrl</code> to complete the submission. 

To submit your app for approval do the following: 

1. Sign into Marketplace with your Atlassian account credentials. 
2. Select **your profile avatar** > **Create an add-on**. 
4. Select **Provide a URL to your artifact (.jar, .obr, .jwb or JSON descriptor)**
5. Click **Enter URL* and add the URL of your app's descriptor.
6. Add the **Name** for your app. Your name should follow the guidelines listed in [Build your presence on Marketplace](https://developer.atlassian.com/market/how-to-market-your-add-on/build-your-presence-on-marketplace) and your app key should be a unique representation of your app’s brand.
7. Choose one of the following options: 
* **Save as Private**: This places your entry into a holding state. We don't review your app until you move it to a Public state. 
* **Next: Make Public**: We will review and list your app on the Bitbucket *Integrations and add-ons* page. 

After you complete your submission, and make it public, our team will review the app and let you know when it will be made available to the public. This process generally takes about 5 business days. 


### App directory

Bitbucket has an [add-on directory](https://bitbucket.org/account/addon-directory) that lists apps produced both by Atlassian and ecosystem developers. It is accessible via the drop down menu linked to your avatar, and via the **Find new add-ons** link on the [Settings](https://bitbucket.org/account/) page.

![add-ons directory](/cloud/bitbucket/images/addons-directory.png)

You can limit the add-on directory to just your app by [appending your app
key as the query parameter `addon`](https://bitbucket.org/account/addon-directory?addon=bitbucket-npm).

For example, `https://bitbucket.org/account/addon-directory?addon=bitbucket-npm` will limit the directory to showing only the **npm for Bitbucket** app. This is useful if you want to send a filtered view of the app directory to a user
wanting to install your app.


