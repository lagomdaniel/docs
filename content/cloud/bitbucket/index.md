---
title: "Latest updates"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: index
date: "2019-04-09"
---

# Latest updates

We deploy updates to Bitbucket Cloud frequently. As a Bitbucket developer, it's important that you're aware of the changes. The resources below will help you keep track of what's happening.

{{% warning title="Important! Deprecation notice for API changes to improve user privacy" %}}

On 12 October 2018, we announced that several Bitbucket Cloud REST APIs are being deprecated in order to improve user privacy. Before continuing, please read the [deprecation notice and migration guide](./bbc-gdpr-api-migration-guide).

{{% /warning %}}

## Recent announcements

Changes announced in the Atlassian Developer blog are usually described in more detail in this documentation. The most recent announcements are documented in detail below:

- [Change notice - Updates to Bitbucket Cloud repository URL paths](/cloud/bitbucket/bitbucket-api-changes-workspaces)
- [Deprecation notice - major changes to Bitbucket Cloud REST APIs to improve user privacy effective 12 October 2018](https://developer.atlassian.com/cloud/bitbucket/bitbucket-api-changes-gdpr/)
- [Deprecation notice - Bitbucket Cloud REST API version 1 is deprecated effective 30 June 2018](/cloud/bitbucket/deprecation-notice-v1-apis/)
- [Announcement - New 2.0 APIs recently released!](/cloud/bitbucket/announcement-06-08-18-new-v2-apis/)
- [Migration Guide - How to adapt to the latest REST API changes regarding privacy] (/cloud/bitbucket/bbc-gdpr-api-migration-guide/)

## Atlassian Developer blog

In the **Atlassian Developer blog** you'll find handy tips and articles related to Bitbucket development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/bitbucket/) *(Bitbucket-related posts)*.

## Bitbucket blog

Major changes that affect all users of the Bitbucket Cloud products are announced in the **Bitbucket blog**. This includes new features, bug fixes, and other changes.

Check it out and subscribe here: [Bitbucket blog](https://blog.bitbucket.org/).