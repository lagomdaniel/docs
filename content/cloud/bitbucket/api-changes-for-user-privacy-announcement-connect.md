---
title: "Major changes to Atlassian Connect APIs are coming to improve user privacy"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: updates
date: "2018-11-28"
---

# Major changes to Atlassian Connect APIs are coming to improve user privacy

Throughout 2018 and 2019, Atlassian will undertake a number of changes to our products and APIs
in order to improve user privacy in accordance with the [European General Data Protection
Regulation (GDPR)]. In addition to pursuing relevant certifications and data handling standards,
we will be rolling out changes to Atlassian Cloud product APIs to consolidate how personal data
about Atlassian product users is accessed by API consumers.

This page summarizes the relevant API changes that we expect to make in the
future. Where possible, we provide a link to specific Jira issues that you can
track to stay up to date about specific changes and when they will go into
effect. We encourage you to watch these issues and *check this page regularly*
in order to stay up to date about any API changes.

## Related announcements

This announcement provides supplementary information to related [Migration guide for major changes to Bitbucket Cloud REST APIs to improve user privacy](../bbc-gdpr-api-migration-guide/).

## Removal of personally identifiable information fields from various APIs

A number of Atlassian Connect APIs accept or provide personally identifiable information (PII) such as user names, user keys and locale information. Such PII fields will be removed from Atlassian Connect APIs and replaced by an Atlassian Account ID field.

## API changes

The following table summarizes the changes that will be made.


|API Area|Changes|Ticket to follow for updates|
|---|---|---|
|Inbound authorization (app → Bitbucket)|The "sub" claim of the OAuth 2.0 JWT Bearer token authorization grant currently requires a user key. Apps will instead need to provide the Atlassian Account ID.|[AC-2409]|
|Outbound authentication (Bitbucket → app)|Atlassian Connect currently sets the JWT "sub" claim to the user key. This will change to the Atlassian Account ID.|[AC-2410]|
|Outbound authentication (Bitbucket → app)|Atlassian Connect currently provides user details in the JWT "user" field of the custom "context" claim. The user details currently include "userKey", "userName" and "displayName". During the deprecation period, a new field will be added to the "user" field containing the Atlassian Account ID. At the end of the deprecation period, the "user" field will be removed completely since the Atlassian Account ID will be available from the "sub" claim.|[AC-2411]|
|App Iframes|Atlassian Connect adds query parameters to the URLs of app iframes in order to provide apps with contextual information. The parameters include user_id, user_key, loc and tz which provide personally identifiable information and will therefore be removed.|[AC-2417]|
|App Iframes|Apps can define URLs with context parameters "profileUser.name" and "profileUser.key". Support for these context parameters will be removed and support for a new context parameter corresponding to the Atlassian Account ID will be added.|[AC-2413]|
|App Iframes|Atlassian Connect creates app iframes with a name parameter containing some personally identifiable information (PII). This PII is not part of our supported API and will be removed from the iframe name parameter.|[AC-2431]|
|App lifecycle|Atlassian Connect adds the "user_key" query parameter to the URLs of the installed and uninstalled lifecycle callbacks. This field is not part of our supported API and will be removed.|[AC-2442]|
|Webhooks|Atlassian Connect adds the "user_key" and "user_id" query parameter to webhook URLs. These fields are not part of our supported API and will be removed.|[AC-2433]|
|Javascript API|The Javascript API provided to app iframes includes an API to get details about the current user: AP.user.getUser(). The details provided include the user's ID, key and full name. The AP.user.getUser() will be deprecated and a new method, AP.user.getCurrentUser() will be created which will return the Atlassian Account ID of the logged in user.|[ACJS-935]|
|Javascript API|The Javascript API provided to app iframes includes an API to get the timezone of the current user: AP.user.getTimeZone(). This API will be removed.|[ACJS-937]|


[European General Data Protection Regulation (GDPR)]: https://www.atlassian.com/blog/announcements/atlassian-and-gdpr-our-commitment-to-data-privacy
[AC-2409]: https://ecosystem.atlassian.net/browse/AC-2409
[AC-2410]: https://ecosystem.atlassian.net/browse/AC-2410
[AC-2411]: https://ecosystem.atlassian.net/browse/AC-2411
[AC-2417]: https://ecosystem.atlassian.net/browse/AC-2417
[AC-2413]: https://ecosystem.atlassian.net/browse/AC-2413
[AC-2414]: https://ecosystem.atlassian.net/browse/AC-2414
[AC-2431]: https://ecosystem.atlassian.net/browse/AC-2431
[AC-2433]: https://ecosystem.atlassian.net/browse/AC-2433
[AC-2442]: https://ecosystem.atlassian.net/browse/AC-2442
[ACJS-935]: https://ecosystem.atlassian.net/browse/ACJS-935
[ACJS-937]: https://ecosystem.atlassian.net/browse/ACJS-937
