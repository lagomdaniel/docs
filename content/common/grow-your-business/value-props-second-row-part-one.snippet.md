<h4 class="center">Use our licensing support</h4>

Marketplace takes care of the licensing and provisioning, so you can focus on what's important: maintaining and improving your app.