<h4 class="center"> We'll handle checkout</h4>

Your <a href="https://developer.atlassian.com/market/marketplace-overview/" target="_blank"> paid-via-Atlassian app </a> is purchasable through our shopping cart system. We'll help you set pricing, and you can forget about the rest.